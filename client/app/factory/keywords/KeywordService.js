 // Factory to hit All Keyword related RESTFUL API===========================*****//
KeywordServices.factory("Keyword",   function($resource) {  // Keywords API*****//
    return $resource('/api/keywords/:id', {
      id: '@id'
      }, {
        keyautocomplete: {
          method:"GET",
          isArray:false
        },
        getKeywordsByKeyid: {
        	method:"POST"
        }
    });
});


KeywordServices.factory("SaveKeyword",   function($resource) {  // Keywords API*****//
    return $resource('/api/savekeywords/:id', {
      id: '@id'
      }, {
       saveKeywords: {
          method:"POST"
        }
    });
});


KeywordServices.factory("GetKeywordsByKey",   function($resource) {  // Keywords API*****//
    return $resource('/api/getkeywordsbykey/:text', {
      text: '@text'
      }, {
        getKeywordsByKeyName: {
          method:"POST"
        }
    });
});