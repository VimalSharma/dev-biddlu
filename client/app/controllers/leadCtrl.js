/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";

//Lead Controller starts here======================================**//

angular.module("myApp.controllers").controller("leadCtrl", function ($scope,GetLead,Lead,Client,LeadDelete,TodaysLeads,PrevSevenDaysLeads,EditLead,EditClient,LeadUpdate,ClientUpdate,$route,$rootScope,$location) {
		
$("#content").hide();

$(window).load(function() {

	$(document).ready(function() {
		var clicked = 0;
		$('.lead-heading').on('click', function() {
			if (clicked === 0) {
				collapse();
				clicked = 1;
			} else if (clicked === 1) {
				expand();
				clicked = 0;
			}
		})

		function collapse() {
			$('#left').animate({
				'width': 'toggle'
			}, 100);
			$('#right').animate({
				'width': '98%'
			}, 100);
		}

		function expand() {
			$('#left').animate({
				'width': 'toggle'
			}, 100);
			$('#right').animate({
				'width': '98%'
			}, 100);
		}
	});
}); //]]>

	//Created By: Sachin Sharma
	//Date:08/08/2014
	//To Get Current Day Date
	var date1 = new Date();
	var date = date1.getDate();
  	var year = date1.getFullYear();
  	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var month = month[date1.getMonth()]; 
	var CurrentDate = date+' '+month+' '+year;
	$scope.todayDate =  CurrentDate;

	//Created By: Sachin Sharma
	//Date:08/08/2014
	//To Get Current Time
	var date = new Date();
	var hours = date.getHours();
  	var minutes = date.getMinutes();
  	var ampm = hours >= 12 ? 'PM' : 'AM';
  	hours = hours % 12;
  	hours = hours ? hours : 12; // the hour '0' should be '12'
  	minutes = minutes < 10 ? '0'+minutes : minutes;
  	var strTime = hours + ':' + minutes + ' ' + ampm;
  	$scope.Time = strTime;

	//Created By: Sachin Sharma
	//Date:04/08/2014
	//Update lead and client information to database
	$scope.UpdateLead = function(JobId){
		//alert(JobId);
		var updatelead  = new LeadUpdate();
		var client = new ClientUpdate();
		
		//------------data to be updated in lead collection------------------------------
		updatelead.JobTitle = $scope.jobtitle;
		updatelead.ApplicationUrl = $("#appurl").val();
		updatelead.MessageUrl = $("#msgurl").val();
		updatelead.Interviews = $scope.interviews;
		updatelead.BidAmount = $scope.amount;
		updatelead.LeadType = $("#ldtype").val();
		//$scope.leadtype;
		updatelead.CurrentStatus = $scope.status;
		updatelead.ModifiedDate = new Date();
		updatelead.JobId =JobId;
		console.log("here it is-> ", updatelead);

 		//-------------data to be updated in client collection---------------
		client.ClientName = $scope.clientname;
		client.ClientEmail = $scope.clientemail;
		client.ClientBudget = $scope.clientbudget;
		client.ClientSkype = $scope.clientskype;
		client.ClientHistory = $scope.history;
		client.ModifiedDate = new Date();
		client.JobId = JobId;
		console.log("client is-> ", client );

        
       
		if(updatelead.JobTitle != "" && typeof updatelead.JobTitle != "undefined" && updatelead.ApplicationUrl != "" && typeof updatelead.ApplicationUrl != "undefined"  && updatelead.Interviews != "" && typeof updatelead.Interviews != "undefined" && updatelead.BidAmount != "" && typeof updatelead.BidAmount != "undefined" && updatelead.LeadType != "" && typeof updatelead.LeadType != "undefined" && updatelead.CurrentStatus != "" && typeof updatelead.CurrentStatus!="undefined" && updatelead.Interviews!=""  && typeof updatelead.Interviews!="undefined" && client.ClientName != "" && typeof client.ClientName != "undefined" && client.ClientBudget != "" && typeof client.ClientBudget != "undefined" && client.ClientHistory != "" && typeof client.ClientHistory != "undefined") {
			var regurl = new RegExp(/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/);
			
			if((regurl.test(updatelead.ApplicationUrl) == true)){
			console.log("in regurl test, console:", updatelead.ApplicationUrl, regurl.test(updatelead.ApplicationUrl));

           // alert(updatelead.MessageUrl);
			if(regurl.test(updatelead.MessageUrl) == false && updatelead.MessageUrl!="")
            {
               $("#msgurl").css("border-color", "red");
 	          alert("Please enter valid Message Url.");
 	          return false;

            }				
 				var regmail = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/);
				if(client.ClientEmail == "" || regmail.test(client.ClientEmail) == true || typeof client.ClientEmail != "undefined"){


               
				var regalpha =  new RegExp(/^[a-z A-Z]*$/);
				if(regalpha.test(client.ClientName) == true){



                 if (check_field(updatelead.Interviews)==false) 
                	{
                        $("#noofinterview").css("border-color", "red");
                		alert("Please enter numeric in No. of Interviews.");
                        return false;
                	};
                if (check_field(updatelead.BidAmount) ==false)
                	{
                        $("#bidamt").css("border-color", "red");
                		alert("Please enter numeric in Bid Amount.");
                        return false;
                	};

                	if (check_field(client.ClientBudget) ==false)
                	{
                         $("#cltbudget").css("border-color", "red");
                		alert("Please enter numeric in Client Budget.");
                        return false;
                	};

				//if((regalpha.test(updatelead.JobTitle) == true) && (regalpha.test(client.ClientName) == true) && (client.ClientSkype == "" || regalpha.test(client.ClientSkype) == true)){
				console.log("regalpha,console:", regalpha.test(client.ClientName) );
 				
 				updatelead.$updateLead(function (response) {
 					console.log("res-->",response);
					if (response.status == 'true') {
						console.log("Lead Updated successfully");
                       
                        if (typeof client.ClientSkype=="undefined") {

                         client.ClientSkype="";

                        };

                         if (typeof client.ClientEmail=="undefined") {

                         client.ClientEmail="";

                        };
						client.$updateClient(function (response) {
							
							if (response.status == 'true') {
								console.log("Client Updated successfully");
								alert($rootScope.Lead_Edit_Msg);
								$scope.jobtitle = ""; 
								$scope.appurl = "";
								$scope.msgurl = "";
								$scope.interviews = "";
								$scope.amount = "";
								$scope.leadtype = "";
								$scope.status = "";
								$scope.id = "";
								$scope.clientname = "";
								$scope.clientemail = "";
								$scope.clientbudget = "";
								$scope.clientskype = "";
								$scope.history = "";
							} else{
								console.log("error",response.error);
								alert($rootScope.invalid_Number_Message);
								return false;
							}
						});
					}
					else{
						console.log("error",response.error);
						alert($rootScope.invalid_Number_Message);
						return false;
					}
				});

			} else{
				alert($rootScope.invalid_Alphabetic_Message);				
				return false;
			}
			} else{

                $("#inputemail").css("border-color", "red");
				alert($rootScope.invalid_Email_Message);
				return false;
			}
			} else{

				$("#appurl").css("border-color", "red");			
				alert($rootScope.invalid_Url_Message);
				return false;
			}
		} else{
			alert($rootScope.required_Fields_Msg);
			$scope.reqFieldsMsg = $rootScope.required_Fields_Msg;
			hideMessage("reqFieldsMsg");

			if (typeof updatelead.CurrentStatus == "undefined" && typeof updatelead.leadtype == "undefined" && typeof client.ClientBudget == "undefined" && typeof client.ClientName == "undefined" && typeof client.ClientHistory == "undefined" && typeof updatelead.JobTitle =="undefined" && typeof updatelead.ApplicationUrl =="undefined" && typeof updatelead.Interviews =="undefined" && typeof updatelead.BidAmount =="undefined") 

			{

				$("#jobtitle").css("border-color", "red");
				$("#appurl").css("border-color", "red");
				$("#noofinterview").css("border-color", "red");
				$("#bidamt").css("border-color", "red");
				$("#clthistory").css("border-color", "red");
				$("#cltname").css("border-color", "red");
				$("#cltbudget").css("border-color", "red");
				$("#ldtype").css("border-color", "red");
				$("#currstatus").css("border-color", "red");
			}

            if (typeof updatelead.JobTitle =="undefined") {

            	$("#jobtitle").css("border-color", "red");
            };
           if (updatelead.ApplicationUrl =="") {

            	$("#appurl").css("border-color", "red");
            };
             if (typeof updatelead.Interviews =="undefined") {

            	$("#noofinterview").css("border-color", "red");
            };
             if (typeof updatelead.BidAmount =="undefined") {

            	$("#bidamt").css("border-color", "red");
            };
             if (typeof client.ClientHistory == "undefined") {

            	$("#clthistory").css("border-color", "red");
            };
             if (typeof client.ClientName == "undefined") {

            	$("#cltname").css("border-color", "red");
            };
             if (typeof client.ClientBudget == "undefined") {

            	$("#cltbudget").css("border-color", "red");
            };
            if ($("#ldtype").val() == "") {               
            	$("#ldtype").css("border-color", "red");
            };
            if (typeof updatelead.CurrentStatus == "undefined") {

            	$("#currstatus").css("border-color", "red");
            };
		}
	}

	$scope.showallleadspaging = function(){
		$("#LoaderDiv").show();
		$("#content").show()
		$("#myleadscontent").hide();
		$("#LoaderDiv").hide();
	}
	$scope.showmyleadspaging = function(){
		$("#LoaderDiv").show();
		$("#content").hide();
		$("#myleadscontent").show();
		$("#LoaderDiv").hide();
	}
	




	$scope.bindjobtitle= function()
	{
	  
       var jobtitle=$location.search().job_title;    
       $scope.jobtitle=jobtitle;
       $( ".modal-backdrop" ).remove();
       $scope.MainJobtitle=jobtitle;
	}
	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Edited Date:30/8/2014
	//Get all Leads from database
	$scope.getAllLeads = function(num){


		//alert(Sessiontest);
		var getlead  = new GetLead();
		var pagenum = num;
		getlead.pagenum = pagenum;
		if($location.search().flag==null)
		{
		getlead.$getAllLeads(function (response) {
			$("#LoaderDiv").show();
			if (response.status != 'false') {
				console.log("response<>>",response);
				var leadsCount = response.count;
				var pagenum = response.pagenum;
				if(pagenum !=0){
						var start = (50*pagenum)-50+1;
						var end = start+50;
						$scope.count = start;

					}else{
						$scope.count = 1;
					}
				var myleadscount = response.myleadscount;
				console.log(myleadscount);
					if (myleadscount <=50){
						var leadpages = 1;
					}
					else{
						var leadpages =  Math.ceil(myleadscount/50);
					}
				//var startnum = response.skipcount;
				$scope.totalLeads = leadsCount;
				console.log("leadsCount",leadsCount);
				var totalPages =  Math.ceil( leadsCount/50);
				$scope.pages = totalPages;
				if(pagenum == 0){

					if(leadsCount>50)
					{
                     $('#content').bootpag({
    				 total: 5,
    				 }).on("page", function(event, num){  $scope.getAllLeads(num)   				
    				 $(this).bootpag({total: totalPages, maxVisible: 5});    
    				 });
					}
					else
					{
                     $('#content').bootpag({
    				 total: 1,
    				 }).on("page", function(event, num){  $scope.getAllLeads(num)   				
    				 $(this).bootpag({total: totalPages, maxVisible: 1});    
    				 });
					}
					if(myleadscount>50)
					{
                     $('#myleadscontent').bootpag({
    				 total: 5
    				 }).on("page", function(event, num){  $scope.getAllLeads(num)
    				 $(this).bootpag({total: leadpages, maxVisible: 5});
     
    				});
					}
					else
					{
                    $('#myleadscontent').bootpag({
    				total: 1
    				}).on("page", function(event, num){  $scope.getAllLeads(num)
    				$(this).bootpag({total: leadpages, maxVisible: 1});
     
    				});
					}
					
				}
				var num = [];
				var a = 0;
				for(var i =0; i<totalPages;i++){
					a=i+1;
					num.push({"index":a});
				}
				$scope.number = num;
				$scope.allleads = response.result.allleads;
				var allleads = response.result.allleads;
				var leadslen = $scope.allleads.length;
				//$scope.totalLeads = leadslen;
				// $scope.biddername = response.uname;
				// var biddername = response.uname;
				// var userLeads = [];
				// for(var i =0; i<leadslen;i++){
				// 	if (allleads[i].BidderName == biddername){
				// 		userLeads.push({"JobUrl":allleads[i].JobUrl,"JobId":allleads[i].JobId,"Status":allleads[i].Status});
				// 	}
				// }

				var specificLength = response.specific.length;
				$scope.myLeadsLen = myleadscount;
				$scope.specificLeads = response.specific;
			}else{
				console.log(response.error);
			}
			$("#LoaderDiv").hide();
		});

		}else if($location.search().flag=='prev'){
	  		$scope.getLastSevendaysLeads();
	  	}
	  	else
	  	{
	  		$scope.getTodaysLeads();
	  	}
	  	
	}

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Get Today's Leads from database
	$scope.getTodaysLeads = function(){
		$scope.count = 1;
		
		var todaysleads  = new TodaysLeads();
		var date = new Date();
		todaysleads.today = date.toISOString();
		todaysleads.$getTodaysLeads(function (response) {
			$("#LoaderDiv").show();
			 if (response.status != 'false') {
			 	$scope.allleads = response.result.leads;
				var allleads = response.result.leads;
				var leadslen = $scope.allleads.length;
				$scope.totalLeads = leadslen;
				var biddername = response.uname;
				var userLeads = [];
				for(var i =0; i<leadslen;i++){
					if (allleads[i].BidderName == biddername){
						userLeads.push({"JobUrl":allleads[i].JobUrl,"JobId":allleads[i].JobId,"Status":allleads[i].Status,"flag":"today"});
					}
				}
				var specificLength = userLeads.length;
				$scope.myLeadsLen = specificLength;
				$scope.specificLeads = userLeads;
				$location.path('/recentleads').search({'flag': 'today'});

			}else{
				console.log(response.error);
			}
			$("#LoaderDiv").hide();
		});
	}	

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Get Previous Seven Days leads from database
	$scope.getLastSevendaysLeads = function(){
		$scope.count = 1;
		var prevleads = new PrevSevenDaysLeads();
		prevleads.$getPrevSevenDaysLeads(function (response) {
			$("#LoaderDiv").show();
			console.log("sevendays leads",response);
			if (response.status != 'false') {
				$scope.allleads = response.result.leads;
				var allleads = response.result.leads;
				var leadslen = $scope.allleads.length;
				$scope.totalLeads = leadslen;
				var biddername = response.uname;
				var userLeads = [];
				for(var i =0; i<leadslen;i++){
					if (allleads[i].BidderName == biddername){
						userLeads.push({"JobUrl":allleads[i].JobUrl,"JobId":allleads[i].JobId,"Status":allleads[i].Status,"flag":"prev"});
					}
				}
				var specificLength = userLeads.length;
				$scope.myLeadsLen = specificLength;
				$scope.specificLeads = userLeads;
				$location.path('/recentleads').search({'flag': 'prev'});
			}else{
				console.log(response.error);
			}
			$("#LoaderDiv").hide();
		});

	}


	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Delete a specific lead from database
	$scope.DeleteLead = function(JobId,flag){
		var leadssdelete  = new LeadDelete();
		var flag = flag;
		leadssdelete.JobId = JobId;
		leadssdelete.flag = flag;
		leadssdelete.$deleteLead(function (response) {
			if (response.status != 'false') {
				$scope.leadDeleteMsg = $rootScope.Lead_Deletion_Msg;
                                $scope.jobtitle = ""; 
								$scope.appurl = "";
								$scope.msgurl = "";
								$scope.interviews = "";
								$scope.amount = "";
								$scope.leadtype = "";
								$scope.status = "";
								$scope.id = "";
								$scope.clientname = "";
								$scope.clientemail = "";
								$scope.clientbudget = "";
								$scope.clientskype = "";
								$scope.history = "";
			 	hideMessage("leadDeleteMsg");
			 	$scope.getAllLeads();
			 	alert($rootScope.Lead_Deletion_Msg);
             } else {
                 alert(JSON.stringify(response));
             }
		});
		
	}

	//Modified By: Vimal Sharma
	//Date:27/10/2014
	//Bind Data to edit form on a link click
	$scope.editLead = function(JobId){

      
		removevalidation();
		console.log("editlead jobid", JobId );
		var editlead = new EditLead();
		var editclient = new EditClient();
		editlead.JobId = JobId;
		editclient.JobId = JobId;
		editlead.$getEditLead(function (response) {
			console.log("response lead--",response, response.data[0]);
			$scope.jobtitle = response.data[0].value.JobTitle;
			$scope.appurl = response.data[0].value.ApplicationUrl;
			$scope.msgurl = response.data[0].value.MessageUrl;
			$scope.interviews = response.data[0].value.Interviews;
			$scope.amount = response.data[0].value.BidAmount;
			$scope.leadtype = response.data[0].value.LeadType;
			$scope.status = response.data[0].value.CurrentStatus;
			$scope.id = response.data[0].value.JobId;
			$scope.clientname = response.data[0].value.ClientName;
			$scope.clientemail = response.data[0].value.ClientEmail;
			$scope.clientbudget = response.data[0].value.ClientBudget;
			$scope.clientskype = response.data[0].value.ClientSkype;
			$scope.history = response.data[0].value.ClientHistory;
		});		
	}

             $scope.remove_jobtitle_validation=function ()
            {
            	
            	$("#jobtitle").css("border-color","" );
            }
             $scope.remove_appurl_validation=function ()
            {
            	            	
            	$("#appurl").css("border-color", "");			
            }
             $scope.remove_interview_validation=function ()
            {
            	
            		$("#noofinterview").css("border-color", "");
            }
             $scope.remove_clthistory_validation=function ()
            {
            	
            	$("#clthistory").css("border-color", "");
            }
             $scope.remove_cltname_validation=function ()
            {
            	
            	$("#cltname").css("border-color", "");
            }
             $scope.remove_cltbudget_validation=function ()
            {
            	
            	$("#cltbudget").css("border-color", "");
            }
             $scope.remove_leadtype_validation=function ()
            {
            	
            	$("#ldtype").css("border-color", "");
            }
             $scope.remove_currstatus_validation=function ()
            {
            	
            	$("#currstatus").css("border-color", "");
            }
            $scope.remove_bidamt_validation=function ()
            {
            	
            	$("#bidamt").css("border-color", "");
            }
           $scope.remove_cltemail_validation=function ()
            {
            	
            	$("#inputemail").css("border-color", "");
            }
$scope.remove_msgurl_validation=function ()
            {
            	
            	$("#msgurl").css("border-color", "");
            }


});

//Created By: Sachin Sharma
//Date:05/08/2014
// function to hide the messages after 4 seconds 
function hideMessage(id) {
  setTimeout(function () {
      $('#'+id).hide();
  }, 4000);
}



//Created By: Sachin Sharma
//Date:07/08/2014
// function to validate Url 
function validUrl(url) {
var url = url;
var rurl = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/
if ((rurl.test(url))){
	var res = true;
}else{
	var res = false;
}
return res;
}



function check_field(id) {   

                if (isNaN(id)) {
                    return false;
                }
            }

            function removevalidation()
            {
            	$("#jobtitle").css("border-color","" );
				$("#appurl").css("border-color", "");
				$("#noofinterview").css("border-color", "");
				$("#bidamt").css("border-color", "");
				$("#clthistory").css("border-color", "");
				$("#cltname").css("border-color", "");
				$("#cltbudget").css("border-color", "");
				$("#ldtype").css("border-color", "");
				$("#currstatus").css("border-color", "");
				$("#inputemail").css("border-color", "");
				$("#inputEmail3").css("border-color", "");
				
            }

