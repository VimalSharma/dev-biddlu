/*jslint node: true */
/*global angular */
"use strict";



 // Factory to hit All Project related RESTFUL API===========================*****//

ProjectService.factory("Project",   function($resource) {

    return $resource('/api/projects/:id', {  // Project API*****//
         id: '@id'
      }, {
        addProject: {  
          method:"POST"
        },
         projectList: {
          method:"GET",
          isArray:false
        },
         removeProject: {
          method:"DELETE"
        },
          updateProject: {
          method: "PUT"
        },
         editProject: {
          method:"POST",
        },
        // projectListByProjectId: {
        //   method:"POST"
        // }
    });
});

ProjectService.factory("ProjectList",   function($resource) {

    return $resource('/api/projectslists/:id', {  // Project API*****//
         id: '@id'
      }, {
        // addProject: {  
        //   method:"POST"
        // },
        //  projectList: {
        //   method:"GET",
        //   isArray:false
        // },
        //  removeProject: {
        //   method:"DELETE"
        // },
        //   updateProject: {
        //   method: "PUT"
        // },
        //  editProject: {
        //   method:"POST",
        // },
        projectListByProjectId: {
          method:"POST"
        }
    });
});