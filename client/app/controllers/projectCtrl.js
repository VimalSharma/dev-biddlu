/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";

// Created By: Parveen Yadav
// Date: 10/07/2014
// Purpose : Create controller for Projects 
angular.module("myApp.controllers").controller("projectCtrl", function ($scope,$timeout, Project, ProjectList,SaveKeyword, ProjectKeyword, ProjectGetKeyword,GetkeyIdByPid,Developer,GetDeveloper,SaveDevelopers, Keyword,DeleteKeyword,$rootScope, $location) {
  var project = new Project();
  var projectKeyword = new ProjectKeyword();
  $scope.arr = [];
  $scope.dev = [];
  $scope.keywordList=[];
  $scope.keys=[];
  $scope.projectId;
  $scope.Developers;
  $scope.developerId;
  $scope.keyArray=[];
  $scope.developerArray=[];
  $scope.keyUpdateArray=[];
  $scope.developerUpdateArray=[];
  $scope.undefinedKeyArray=[];
  $scope.projects=[];
  var uniqueProjects=[];
   $("[rel='tooltip']").tooltip();
  $('[data-toggle="tooltip"]').tooltip();

  $scope.tasksModal = function(event, projects) {
 
    console.log(event.clientY);
    var yaxix=event.clientY-130;
    console.log(yaxix);
 
    $('#myModal').modal('show');
     setTimeout(function() {
      $('#myModal').modal('hide');
      $scope.$apply();
    }, 2000);
     console.log(projects);
    $("#myModal").css('top',yaxix);
  }
  $scope.close = function() {
    //$('#myModal').modal('hide');
  }



  $scope.GetAllProjects = function() {
    $scope.projectsAll = [];
    //var pagenum = num;
    var projects = new Project();
    
    //projects.pagenum = pagenum;
    projects.$projectList(function(response) {
      console.log("get all projects function", response , response.project.length);
      if (response.status != 'false') {
        $scope.projectsAll = response.project;
        //var projectsCount = response.project.length;
        //var pagenum = response.pagenum;
        //var totalProjectsdata = response.result.allbids;
      /* $scope.projects.push(response.project);
      } else { */
      }
    });
  };





    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: Intilize function to Add a New Project

    // Modified By: Sachin Sharma
    // Date: 24/07/2014
    // Puropse: Intilize function to Add a New Project

      $scope.AddProject = function (title, url, githubUrl, description, keywords, developers) {



        
        project.Title = title;
        project.Url = url;
        project.GitUrl = githubUrl;
        project.Description = description;
        project.CreatedDate =  new Date();

        
      //For Validation
       $scope.isValidate(title, url, githubUrl, description, keywords, developers,function(validate){
     

      if(validate==true)
      {
         //Loop to save all keyword ID from JSON data        

           
                  angular.forEach(keywords, function (value, key) {
             
                        if(typeof keywords[key]["id"] != 'undefined')
                         { 
                          
                           $scope.keyArray.push(keywords[key]["id"]);

                         }
                         else
                         {

                           var Keyobj=new SaveKeyword();

                           Keyobj.text=keywords[key]["text"];

                           Keyobj.$saveKeywords(function(response){
                           
                             $scope.keyArray.push(response.keywords["id"]);
                           });
                        }
                     });
                  
            

         var waitToRenderKeywordId = $timeout(function myFunction() {
            
                 //Loop to save all Developer ID from JSON data           
                 angular.forEach(developers, function (value, key) {
                    
                    $scope.developerArray.push(developers[key]["id"]);
                   
                });
       
 

         project.KeywordsId=$scope.keyArray;
         project.DevelopersId =$scope.developerArray;


            project.$addProject(function (response) {

                if (response.status != 'false') {

                 //Loop to hit all keywords ID from JSON and then add them in ProjectKeywords schema
                 angular.forEach($scope.keyArray, function (value, key) {
                    $scope.AddProjectKeywords($scope.keyArray[key], response.id);

                 });



                   //Loop to hit all developer ID from JSON and then add them in ProjectKeywords schema            
                  angular.forEach(developers, function (value, key) {

                       $scope.AddProjectDevelopers(response.id,developers[key]["id"]);

                  });



                  // projectKeyword.$projkeynamejoin();

                    alert($rootScope.Project_Add_Msg);
                    
                     $scope.projectId.project.Title="";
                     $scope.projectId.project.Url="";
                     $scope.projectId.project.GitUrl="";
                     $scope.projectId.project.Description="";
                     $scope.keywordList=[];
                     $scope.DeveloperList=[];


                } else {

                    alert(response);

                }
            });

        },1000);
    }
  



});
    };



    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: Delete  Projects
    $scope.DeleteProject = function (project_id, project_data) {  
      var project = new Project();
      project.id = project_id
      project.$removeProject(function (response) {
        if (response.status != 'false') {
          var idx = $scope.projects.indexOf(project_data);              
          $scope.projects.splice(idx, 1);                   
          alert($rootScope.Project_Deletion_Msg);
        } else {
          alert(JSON.stringify(response));
        }
      });
    };

    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: Get Project according to project id
    $scope.EditProject = function (project_id) {
      $scope.arr = [];
      var project = new Project();
      project.id = project_id
      project.$editProject(function (response) {
        if (response.status != 'false') {
          $scope.projectId = response;    
          $scope.GetKeywordsByProjectId(project_id);
        } else {
          //alert(response);
        }
      });
    };
    
    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: Update Project according to project id
    $scope.UpdateProject = function (project_id, description, url, giturl, keywords, developer, title) {
      var project = new Project();
      project.id = project_id;
      project.Title = title;
      project.Url = url;
      project.GitUrl = giturl;
      project.Description = description;
      project.ModifyDate =  new Date(); 
      //Check Validation
      $scope.isValidate(title, url, giturl, description, keywords, developer,function(validate){
      if(validate==true)
      {
        //Loop to save all keyword ID from JSON data           
        angular.forEach(keywords, function (value, key) {      
          $scope.keyUpdateArray.push(keywords[key]["id"]);      
        });

        //Loop to save all Developer ID from JSON data           
        angular.forEach(developer, function (value, key) {
          $scope.developerUpdateArray.push(developer[key]["id"]);
        });
        project.KeywordsId=$scope.keyUpdateArray;
        project.DevelopersId =$scope.developerUpdateArray;
        project.$updateProject(function (response) {
          if (response.status != 'false') {     
          $scope.DeleteKeywordsIdByProjectId(project_id);  
            // Loop to hit all developer ID from JSON and then add them in ProjectKeywords schema            
            angular.forEach(developer, function (value, key) {
              $scope.AddProjectDevelopers(response.id,developer[key]["id"]);
            });

            //Loop to hit all keywords ID from JSON and then add them in ProjectKeywords schema
            angular.forEach(keywords, function (value, key) {
              $scope.AddProjectKeywords(keywords[key]["id"], response.id);
            });
            alert($rootScope.Project_Updation_Msg);
          } else {
            alert(response);
          }
        });
      }
    });    
  };

    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: function to Add a New Keywords

    $scope.AddProjectKeywords = function (keyword_Id, project_Id) {
      var projectKeyword = new ProjectKeyword();
      projectKeyword.projectId = project_Id;
      projectKeyword.keywordId = keyword_Id;
      projectKeyword.$addKeywords(function (response) {
      });
    };

    // Created By: Parveen Yadav
    // Date: 25/07/2014
    // Puropse: function to Add a New Developers

    $scope.AddProjectDevelopers = function (project_Id,developer_Id) {
      var obj = new SaveDevelopers();
      obj.projectId = project_Id;
      obj.developerId = developer_Id;
      obj.$addDevelopers(function (response) {
      });
    };

    // Modified By: Sachin Sharma
    // Date: 23/07/2014
    // Puropse: function to get a Keywords by  project id
    $scope.GetKeywordsByProjectId = function (project_Id,project) {

        $scope.keys=[];

        var getkeyIdByPid = new GetkeyIdByPid();

        getkeyIdByPid.projectId = project_Id;

        getkeyIdByPid.$getKeywordsIdByPid(function (response) {
          
            angular.forEach(response.keywordsId, function (value, key) {

                $scope.GetAllKeywords(response.keywordsId[key]["keywordId"]);

                $scope.GetAllDevelopers(response.keywordsId[key]["developerId"]);

            });

        });
    };

    // Created By: Sachin Sharma
    // Date: 24/07/2014
    // Puropse: function to get Developer name

    $scope.GetAllDevelopers = function (developerId) {
      var developer  = new GetDeveloper();
      $scope.dev=[];
      developer.id = developerId;
      developer.$getDeveloperBydevid(function (response) {
        // $scope.projects.DeveloperList = [response.DeveloperList];
        $scope.dev.push(response.DeveloperList);
        $scope.DeveloperList=$scope.dev;
      });
    };

    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: function to get all Keywords

    $scope.GetAllKeywords = function (keywordId) {
      $scope.keys = [];
      var keyword = new Keyword();
      keyword.id = keywordId;
      keyword.$getKeywordsByKeyid(function (response) {
        $scope.keys.push([response.keywordList]);
        $scope.arr.push(response.keywordList);
        $scope.keywordList=$scope.arr; 
      });      
    };

    // Created By: Vimal Sharma
    // Date: 29/10/2014
    // Puropse: Function to get projects according to keywordname on seraching
    
   $scope.GetAllProjectByKeyId = function (keyword_id){

    $scope.projects=[];
    $scope.Noprojects="";
    uniqueProjects=[]
    
   if (keyword_id==null || keyword_id=="" || keyword_id=='undefined') {
     
      $scope.projects=[];
        alert($rootScope.search_msg);
   
      }   
      else
      {

     var keyword_name=JSON.stringify(keyword_id[0]["text"]);

        var projectGetKeywords = new ProjectGetKeyword();
        projectGetKeywords.keywordId  = keyword_id;
        projectGetKeywords.$getprojectsIdByKeyid(function (response) {
          for(var i = 0;i<response.data.length;i++){  
           if(response.data[i].length>0){           
              for(var j = 0;j<response.data[i].length;j++ ){
                if(response.data[i][j].value.Developers)
                {
                  var Dev="";
                  for (var d = 0; d <response.data[i][j].value.Developers.length; d++) {
                    if(d!=0)
                    {
                      Dev = Dev.concat(" , ");
                    }
                    Dev = Dev.concat(response.data[i][j].value.Developers[d]);
                    
                  };
                  response.data[i][j].value.Developers=Dev;
                }
                uniqueProjects.push(response.data[i][j].value);
              }
            }
            else
            {
              $scope.Noprojects=$rootScope.Project_Search_Err;
            }
          }
          for (var p = 0; p < uniqueProjects.length; p++)
          {     
            $scope.projects.push(uniqueProjects[p]);        
          }  
        });
      }
    }
  
    // Created By: Parveen Yadav
    // Date: 15/07/2014
    // Puropse: function to get all search Projects according to their Keyword Id

    // Modified By: Sachin Sharma
    // Date: 23/07/2014
    // Puropse: function to get all search Projects according to their Keyword Id
    $scope.GetAllProjectsbyProjectId = function (project_id) {
      $scope.projects=[];
      var projectLists = new ProjectList();
      projectLists.id = project_id; 
      projectLists.$projectListByProjectId(function (response) {
        if (response.status != 'false') {                
          $scope.projects.push(response.project);       
        } else {
          //alert($rootScope.Project_Search_Err);
        }
      });
    };


    $scope.remove_title_validation=function ()
            {
              
              $("#exampleInputEmail1").css("border-color", "");
            }

            $scope.remove_url_validation=function ()
            {
              
              $("#exampleInputPassword1").css("border-color", "");
            }

            $scope.remove_github_validation=function ()
            {
              
              $("#exampleInputPassword").css("border-color", "");
            }

             $scope.remove_desc_validation=function ()
            {
              
              $("#description").css("border-color", "");
            }


    // Modified By: Parveen Yadav
    // Date: 31/07/2014
    // Puropse: function to delete all  keywords by their project Id
    $scope.DeleteKeywordsIdByProjectId = function (project_id) {

       var obj=new DeleteKeyword();

        obj.projectId = project_id;

        obj.$deletekeywordsIdByProjectId(function (response) {

            if (response.status == 'true') {
               
            } else {
            }
        });
    };

     // Modified BY: Parveen Yadav
     // Date : 11/08/2014
     // Purpose : Copy to clipboard 

    $scope.getTextToCopy = function(url,description,id) {
        var str = '';   
        var id = id;
        var projectUrl = url;
        var description = description;
        console.log("URL"+url,+"description"+description,"Id"+id);

        str += "URL: "+projectUrl +"\n";
        str += "Description: "+ description+"\n";
        str += "Id: "+ id+"\n";
        
      //  alert(str)
        return (str);
    }

   
      $scope.copyToClipBoard = function (url,description,id) {
        console.log("URL"+url,+"description"+description,"Id"+id);

    }



    $scope.doSomething = function () {
        console.log("Copied...");
    }




    // Created By: Parveen Yadav
    // Date: 06/08/2014
    // Puropse: Redirect to Addproject Page to Update Project

      $scope.ReturnToAddProject = function (project_id) {
       $location.path('/addproject').search({'p_id': project_id});
    };



    // Created By: Parveen Yadav
    // Date: 06/08/2014
    // Puropse: Redirect to Addproject Page to Update Project

      $scope.BindProject = function () {
    
         if($location.search().p_id==null)
         {
                   $('#add_proj_form').hide();
                   $('a#add_project').click(function () {
                   $('#add_proj_form').show(400);
            });
                   $('#save').show();
                   $('#update').hide();
                    //$('#addProject').addClass('active');

         }
         else
         {         $('#add_proj_form').show();
                   $('#save').hide();
                   $('#update').show();
                   // $('#addProject').addClass('active');
                   $scope.EditProject($location.search().p_id);
         }
    };




    // Created By: Parveen Yadav
    // Date: 06/08/2014
    // Puropse: Validation  for  Addproject and  Update Project

    $scope.isValidate = function (title, url, githubUrl, description, keywords, developers,callback) {
       var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
       if((title=='undefined' || title=='' || title==null) || (url=='undefined' || url=='' || url==null)  || (githubUrl=='undefined' || githubUrl=='' || githubUrl==null)  || (description=='undefined' || description=='' || description==null)||(keywords.length<=0) ||(developers.length<=0))
        {
          alert($rootScope.form_Empty_Message);

          if(title=='undefined' || title=='' || title==null){
            $("#exampleInputEmail1").css("border-color", "red");
          } 
          if(url=='undefined' || url=='' || url==null){
            $("#exampleInputPassword1").css("border-color", "red");
          } 
          if(githubUrl=='undefined' || githubUrl=='' || githubUrl==null){
            $("#exampleInputPassword").css("border-color", "red");
          } 
          if(description=='undefined' || description=='' || description==null){
            $("#description").css("border-color", "red");
          }
          if(keywords.length<=0){
           $("#keywordtest").css("border-color", "red");
          }
          if(developers.length<=0){
          $("#developer").css("border-color", "red");
          }

          callback(false);
        } else {
         //  if(urlregex.test(url)==false)
         //  {
         //     alert("Please enter a valid url.");
         //     $("#exampleInputPassword1").css("border-color", "red");
         //     callback(false);
         //     //return false;
         //  }
         
          
         // if(urlregex.test(githubUrl)==false)
         //  {
         //     alert("Please enter a valid github url.");
         //      $("#exampleInputPassword").css("border-color", "red");
         //      callback(false);
         //      //return false;
         //  }
         if(urlregex.test(url) && urlregex.test(githubUrl))
          {
           callback(true);
          }
          else
          {

             if(urlregex.test(url)==false)
          {
             alert("Please enter a valid Project url.");
             $("#exampleInputPassword1").css("border-color", "red");
            // callback(false);
             return false;
          }
         
          
         if(urlregex.test(githubUrl)==false)
          {
             alert("Please enter a valid github url.");
              $("#exampleInputPassword").css("border-color", "red");
             // callback(false);
              return false;
          }
             // alert("Please enter a valid url.");
             callback(false);
          }

        }        
    };

    
});

