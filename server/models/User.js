var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = new Schema({
	UserName: String,
	Password: String,
	Role: String
}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

mongoose.model('User', User);



