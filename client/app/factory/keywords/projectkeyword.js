/*jslint node: true */
/*global angular */
"use strict";


  // Factory to hit All Keyword related RESTFUL API===========================*****//
ProjectKeywordServices.factory("ProjectKeyword",   function($resource) {  // Keywords API*****//
    return $resource('/api/projectkeywords/:id', {
       id:'@id'
      },{
        addKeywords: {
          method:"POST"
        }
      });
    });


ProjectKeywordServices.factory("DeleteKeyword",   function($resource) {  // Keywords API*****//
    return $resource('/api/deletekeywords/:projectId', {
       projectId:'@projectId'
      },{
        deletekeywordsIdByProjectId:{
          method:"DELETE"
        }
  });
});



ProjectKeywordServices.factory("ProjectGetKeyword",   function($resource) {  // Keywords API*****//
    return $resource('/api/projectgetkeywords/:id', {
       id:'@id'
      },{
        getprojectsIdByKeyid: {
          method: "POST",
          isArray:false
        }
    });
});


ProjectKeywordServices.factory("GetkeyIdByPid",   function($resource) {  // Keywords API*****//
    return $resource('/api/getkeyidbypid/:id',{
       id:'@id'
      },{
        getKeywordsIdByPid: {
          method: "POST",
          isArray:false
        },
    });
});



ProjectKeywordServices.factory("SaveDevelopers",   function($resource) {  // Keywords API*****//
    return $resource('/api/savedevelopers/:id',{
       id:'@id'
      },{
        addDevelopers: {
          method: "POST"
        }
    });
});


