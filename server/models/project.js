var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var keywordList=mongoose.model('Keyword');
var developer=mongoose.model('Developer');

var Project = new Schema({
	Title: String,
	Url: String,
  GitUrl: String,
  Description: String,
  KeywordsId: [],
  DevelopersId: [],
  CreatedDate: Date,
  ModifyDate: Date,
  Deleted: Boolean,
	IsActive: Boolean
}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

mongoose.model('Project', Project);
