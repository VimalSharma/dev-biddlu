/*jslint node: true */
/*global angular */
"use strict";



 // Factory to hit All Workroom related RESTFUL API===========================*****//

WorkroomService.factory("Workroom",   function($resource) {

    return $resource('/api/workrooms/:id', {  // Workroom API*****//
      id: '@id'
      }, {
        addJob: {  
          method:"POST"
        },
         getAllJobs: {
          method:"GET",
          isArray:false
        },
        removeJobs: {
          method:"DELETE"
        }
    });
})

WorkroomService.factory("WorkroomGetJob",   function($resource) {

    return $resource('/api/workroomsgetjob/:id', {  // Workroom API*****//
      id: '@id'
      }, {
        
          getJobByUrl: {
           method:"POST",
         },
    });
})