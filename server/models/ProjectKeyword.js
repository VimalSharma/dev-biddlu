var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId=mongoose.Schema.ObjectId;


var ProjectKeyword = new Schema({
  projectId: String,
  keywordId: String,
  developerId: String,


}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

module.exports=mongoose.model('ProjectKeyword', ProjectKeyword);

