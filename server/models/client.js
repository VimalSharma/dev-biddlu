var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Client = new Schema({
  JobId : String,
  ClientName: String,
  ClientEmail: String,
  ClientBudget: Number,
  ClientSkype: String,
  ClientHistory:String,
  date: Date,
  ModifiedDate:Date,
}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

mongoose.model('Client', Client);