var mongoose = require('mongoose'),Lead=mongoose.model('Lead');
//var client = mongoose.model('Client');
var ClientLead = mongoose.model('client_lead_join'), Bids = mongoose.model('Bids');
var moment = require('moment');
mongoose.set('debug', true);

  	//Created By: Sachin Sharma
   	//Dated: 31/7/2014
   	// Save lead data in mongoose database
  	exports.saveLead = function(req, res) {
    	console.log("here 1==================s",req.body);

      var document = new Lead(req.body);
      document.save(function(err, lead) {
        if (err) {
          res.json({"status":"false","error":err}); 
        }else{
          res.json({"status":"true","lead":lead});
        }
      });
    };

// Lead.find({}, function(err, project_list){    //monoose query to find all data
//             var json={};
//             if((err==null ) && (project_list!=null)){
//               json={"leads":project_list};
//               console.log(json);
//                 res.send({"status":"true"},json);  
//                }else{
//               res.send({"status":"false","error":err}); 
//             }
//           });
//*******saveLead function ends here*******//


    
    //Created By: Sachin Sharma
    //Dated: 31/7/2014
    // Get all Leads from leads collection
    exports.getAllLeads = function(req, res) {
      var username = req.session.username;
      var datalimit = 50;
        Bids.find({"BidType":"lead"}).count(function (e, count) {
          var count = count;
          Bids.find({"BidType":"lead","BidderName":username}).count(function (err, myleadscount) {
            if((err==null ) && (myleadscount!=null))
              var myleadscount = myleadscount;

          if(typeof req.body != undefined || req.body != ''){
            if(req.body.pagenum) {
            var pagenum = req.body.pagenum; var skipcount = (pagenum*datalimit)-datalimit;
            }else{
            var pagenum = 0;
            var skipcount = 0;
            }   
          }else{
            var pagenum = 0;
            var skipcount = 0;
          }
          
          Bids.find({"BidderName":username,"BidType":"lead"}).limit(datalimit).skip(skipcount).sort({date:-1}).exec(function(err, specificleads) {
            if((err==null ) && (specificleads!=null))
              data={"specificleads":specificleads}

          Bids.find({"BidType":"lead"}).limit(datalimit).skip(skipcount).sort({date:-1}).exec(function(err, allleads) {
            var json={};
            if((err==null ) && (allleads!=null)){
              json={"allleads":allleads}
              res.json({"status":"true","result":json,"uname":username,"count":count,"specific":specificleads,"pagenum":pagenum,"myleadscount":myleadscount});  
            }
            else{
              res.send({"status":"false","error":err}); 
            }
          });
        });
      });
    });
  }

    //Created By: Sachin Sharma
    //Dated: 30/7/2014
    // Get today's leads from bids collection
    exports.getTodaysLeads = function(req, res) {
      var date = new moment();
      //var newdate = new moment();
      //console.log("new",newdate.toDate());
      var str = " 00:00:00";
      var tdate = date.format('YYYY-MM-DD') + str;
      var username = req.session.username;
      var newdate = new moment(tdate);

      console.log(newdate);
      Bids.find({"BidType":"lead","date": { $gte: newdate.toDate() }}).sort({date:-1}).exec(function(err,leads) {
        var json={};
          if((err==null ) && (leads!=null)){
              json={"leads":leads}
              console.log(json);
              res.json({"status":"true","result":json,"uname":username});  
          }
          else{
            res.send({"status":"false","error":err}); 
          }
      });
    }

    //Created By: Sachin Sharma
    //Dated: 31/7/2014
    // Get Previous Seven days  leads from bids collection
    exports.getPrevSevenDaysLeads = function(req, res) {
      console.log("here inside get previous seven",req.body);
      var date = new moment();
      var newdate = new moment();
      var str = " 00:00:00";
      var tdate = date.format('YYYY-MM-DD') + str;
      var username = req.session.username;
      var newdate = new moment(tdate);
      newdate.subtract('days', 7);
      Bids.find({"BidType":"lead","date": { $gte: newdate.toDate() }}).sort({date:-1}).exec(function(err,leads) {
        var json={};
        if((err==null ) && (leads!=null)){
            json={"leads":leads}
            console.log(json);
            res.json({"status":"true","result":json,"uname":username});  
        }
        else{
            res.send({"status":"false","error":err}); 
        }
      });
    }

    //Created By: Sachin Sharma
    //Dated: 31/7/2014
    // Change Bid type from  lead to deleted
    exports.deleteLead = function(req, res) {
      console.log(req.params,"sgvdfhgfhg",req.body);
      var JobId = req.body.JobId;
      var type = "Deleted";
      Bids.update({"JobId":req.params.JobId},{$set:{BidType: type}},function(err, result) {
        if (err) {
          res.json({"status":"false","error":err}); 
        }else{
          res.json({"status":"true","data":result});
        }
      });
    }

    //Created By: Sachin Sharma
    //Dated: 04/08/2014
    // Get lead data using jobid
    exports.getEditLead = function(req, res) {
      //console.log("getEditLead req.params.JobId--",req.params.JobId,"getEditLead body",req.body);
      ClientLead.find({"value.JobId":req.params.JobId},function(err, result) {
        //console.log("err ", err, "-----result ", result);
        if (err) {
          res.json({"status":"false","error":err}); 
        }else{
          res.json({"status":"true","data":result});
        }
      });
    }

    //Created By: Sachin Sharma
    //Dated: 04/08/2014
    // Edit/Update lead data using jobid
    exports.updateLead = function(req, res) {
      console.log("sachin",req.body);
      var JobTitle = req.body.JobTitle;
      var AppUrl = req.body.ApplicationUrl;
      var MsgUrl = req.body.MessageUrl;
      var interviews = req.body.Interviews;
      var amount = req.body.BidAmount;
      var LeadType = req.body.LeadType;
      var status = req.body.CurrentStatus;
      var date = req.body.ModifiedDate;
      
      Lead.update({"JobId":req.params.JobId},{$set:{JobTitle: JobTitle,ApplicationUrl:AppUrl,MessageUrl:MsgUrl,
        Interviews:interviews,BidAmount:amount,LeadType:LeadType,CurrentStatus:status,ModifiedDate:date}},function(err, result) {
          console.log("err ", err, "-------result ", result);
          if (err) {
            res.json({"status":"false","error":err}); 
          }else{
            res.json({"status":"true","data":result});
          
          }
        });
      }

     exports.Getleads = function(req, res) {
        Lead.find().sort({date:-1}).exec(function(err,project_list) {    //monoose query to find all data
            var json={};
          if((err==null ) && (project_list!=null)){
            json={"project":project_list}
              res.send({"status":"true"},json);  
             }else{
            res.send({"status":"false","error":err}); 
          }
        });
      };