/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";

//Bid Controller starts here======================================**//
angular.module("myApp.controllers").controller("bidCtrl", function($scope, Bid, GetBids, Lead, Client, BidDelete,DuplicateBid,ChangeBidStatus, ChangeBidType, TodaysBids, PrevSevenDaysBids,BidByJobUrl,SearchJobUrl,ClientLead, $route, $rootScope, $location,$window) {

     $("[rel='tooltip']").tooltip();
     $('[data-toggle="tooltip"]').tooltip();
	 $("#content1").hide();
   
	//Created By: Sachin Sharma
	//Date:08/08/2014
	//To Get Current Day Date
	var date1 = new Date();
	var date = date1.getDate();
	var year = date1.getFullYear();
	var month = new Array();
	month[0] = "January";
	month[1] = "February";
	month[2] = "March";
	month[3] = "April";
	month[4] = "May";
	month[5] = "June";
	month[6] = "July";
	month[7] = "August";
	month[8] = "September";
	month[9] = "October";
	month[10] = "November";
	month[11] = "December";
	var month = month[date1.getMonth()];
	var CurrentDate = date + ' ' + month + ' ' + year;
	$scope.todayDate = CurrentDate;

	//Created By: Sachin Sharma
	//Date:08/08/2014
	//To Get Current Time
	var date = new Date();
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'PM' : 'AM';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	$scope.Time = strTime;

	//Created By: Sachin Sharma
	//Date:28/7/2014
	// Options for status
	$scope.Status = [{
		name: 'Applying',
		value: 'Applying'
	}, {
		name: 'Applied',
		value: 'Applied'
	}];

	//$scope.selectedstatus = $scope.Status[0].value;

	//Created By: Sachin Sharma
	//Date:28/7/2014
	// Options for IsInvite
	$scope.invitation = [{
		name: 'No',
		value: 'No'
	}, {
		name: 'Yes',
		value: 'Yes'
	}];


	$scope.tech = [{
  name: 'Mobile',
  value: 'Mobile'
 }, {
  name: 'Web',
  value: 'Web'
 }];
 $scope.tech = $scope.tech[1].value;

	$scope.selectedinvite = $scope.invitation[0].value;

	//Created By: Sachin Sharma
	//Date:31/7/2014
	// Options for LeadType
	$scope.Type = [{
		name: 'Fixed',
		value: 'Fixed'
	}, {
		name: 'Hourly',
		value: 'Hourly'
	}];

	$scope.leadtype = $scope.Type[0].value;



	

	//Created By: Sachin Sharma
	//Date:28/7/2014
	//To get the JobPortal type and jobid from url
	$scope.getJobPortalAndId = function() {


        var regurl = new RegExp(/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/);
		var url = $("#joburl").val().trim();

		console.log("Complete url:", url);	
        if (url=="") {
          $("#joburl").css("border-color","" );      	
          return false;
        }  
       
		if (url!="" && regurl.test(url)==false) 
		{  
            
            alert("Enter proper odesk or elance url.");
			$("#joburl").css("border-color","red" );
			$("#jobportal").val('');
			$("#jobid").val('');
			return false;
		}
		else
 		{	

 				 
            //if (url.contains("odesk") && url.contains("elance") ) {             		
			// $("#joburl").css("border-color","red" );
			// $("#jobportal").val('');
			// $("#jobid").val('');
			// return false;
		//}
		if (url.indexOf("www.odesk.com") > 0) {	
	
				$("#jobportal").val('odesk');
			} else if (url.indexOf("www.elance.com") > 0) {			
				$("#jobportal").val('elance');
			} else {			
				$scope.portal = '';
				 alert("Enter proper odesk or elance url.");
			    $("#joburl").css("border-color","red" );
			    $("#jobportal").val('');
			    $("#jobid").val('');
			    return false;
			}
		}		
		 var id = getid(url);
		$("#jobid").val(id);
 $("#joburl").css("border-color","" );
}


//Created By: Shweta Saluja
//Date:13/3/2015
//Open Bid Url in Browser Window.
$scope.OpenBid = function() {
  var JobId = document.getElementById('hidjob').value;
  var url = document.getElementById('hidurl').value; 
  $window.open(url);
 }


$scope.filterbid=function()
{
   $("#send").show();	
   var regurl = new RegExp(/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/);
	var srchlead=$("#srch_lead").val().trim();   
        if (srchlead!="" && regurl.test(srchlead)==true) 
        	{
                var id = getid(srchlead);
                var dupbids = new DuplicateBid();
                  dupbids.JobId=id;
                dupbids.$duplicatebid(function(response) {
                 	console.log(response);
                     
                 	  if (response.counts == 0) 
                 	  {
                 	  	 $scope.specificBids=""; 
					     $scope.allbids = "";

                         $("#joburl").val(srchlead);    
                         if (srchlead.indexOf("www.odesk.com") > 0) {				
				            $("#jobportal").val('odesk');
			                } 
			                if (srchlead.indexOf("www.elance.com") > 0) {			
				          $("#jobportal").val('elance');
			                  }
                         var id = getid(srchlead);
		               $("#jobid").val(id);
		               $("#srch_lead").val('');


                 	  }
                 	  else
                 	  {

                 	  	alert("This Job already Exists");
                 	  	//$scope.Bids_Status();
                 	  	return false;
                 	  }
                 });
     
     		
        	}
        	else
        	{

                 if (srchlead=="") 
                    {

                    	alert("Please enter Bid url.");
                    	
                    	return false;
                    }
                 if(regurl.test(srchlead)==false)
                 {

                        alert("Enter proper odesk or elance url.")
                      
                    	return false;
                 }
        	}
}

$scope.SaveBid = function() {

  $("#allbidslist").css("display","none");
  $("#mybidslist").css("display","none");
  var bids = new Bid();
  bids.BidderName = $scope.biddername;
  bids.Technology = $scope.tech;
  bids.JobUrl = $("#joburl").val().trim();
  bids.JobPortal = $("#jobportal").val();
  bids.JobId = $("#jobid").val();
  bids.IsInvite = $scope.selectedinvite;
  bids.Status = $scope.selectedstatus;
  var bidsstatus=$("#status").val();
  
  bids.BidType = "bid";
  bids.date = new Date();
  var joburl = bids.JobUrl;
  if (bids.JobUrl && bids.JobId && bids.JobPortal && bidsstatus !="") {
   var url = validateUrl(joburl);
   if (url == true) {

    if (bids.Status != null) {
    	
     $("#LoaderDiv").show();
     bids.$savebid(function(response) {
     
      if (response.status == 'true') {

      	
       $scope.bidsText="";
       $("#status").css("border-color", "#ccc");
       $("#exampleInputEmail1").css("border-color", "#ccc");
       $scope.bidaddMsg = $rootScope.Bid_Add_Msg;
       alert($rootScope.Bid_Add_Msg);
       document.getElementById("joburl").value = "";
       document.getElementById("jobportal").value = "";
       document.getElementById("jobid").value = "";

       $("#status").val(-1).attr("selected", "selected");
        $scope.getAllBids();
      
      } else {
      	
       $("#LoaderDiv").hide();
       $scope.jobExistMsg = $rootScope.Job_Exist_Msg;
       alert($scope.jobExistMsg);
       hideMessage("jobExistMsg");
      }
     });
    } else {

           if (typeof bids.Status=="undefined" ||  bids.Status==null || bidsstatus == "") {
       	$("#status").css("border-color", "red");          
         alert("Please Select Status.");
         return false;
          };
    	
     
    }

   } else {

   
    $("#exampleInputEmail1").css("border-color", "red");
    $scope.invalidMsg = $rootScope.invalid_Odesk_Url_Message;
    hideMessage("invalidMsg");
   }
  } else {  


       if (typeof bids.JobUrl=="undefined" || bids.JobUrl=="") 
        	{$("#joburl").css("border-color", "red"); 
            alert("Please Fill Job Url.");
             return false;
        	 };

       if (typeof bids.Status=="undefined" ||  bids.Status==null || bidsstatus == "") {
       	$("#status").css("border-color", "red");          
         alert("Please Select Status.");
         return false;
          };

     }
 }

 $scope.removestatusvalidation=function()
 {

 	$("#status").css("border-color", ""); 
 }

	//Modified By: Vimal Sharma
	//Date:15/11/2014

	$scope.showallbidspaging = function() {
		$("#LoaderDiv").show();
			$("#content1").show();
			$("#mybidscontent").hide();

			$scope.allbids = "";
        document.getElementById("joburl").value = "";
       document.getElementById("jobportal").value = "";
       document.getElementById("jobid").value = "";
        
       $("#status").val(-1).attr("selected", "selected");
          //  alert("hello");	
          var srchlead=$("#srch_lead").val();
						//alert("test........ "+srchlead);
						if (srchlead=="") 
							{		
			var getbids = new GetBids();
			if ($location.search().flag == null) {
				getbids.$getAllBids(function(response) {

					if (response.status != 'false') {
						//console.log("responsecvbbbbbbb", response);
						var bidsCount = response.count;
						var mybidscount = response.mybidscount;
						var pagenum = 0;
						var totalBidsdata = response.result.allbids;
						if (pagenum != 0) {
							var start = (50 * pagenum) - 50 + 1;
							var end = start + 50;
							$scope.count = start;
						} else {
							$scope.count = 1;
						}

						if (mybidscount <= 50) {
							var bidspages = 1;
						} else {
							var bidspages = "";
							var remainder = mybidscount % 50;
							var pagecount = mybidscount / 50;
							if (remainder <= 49) {
								bidspages = pagecount + 1;
							};
						}
						$scope.totalBids = bidsCount;						
						//console.log("bidsCount", bidsCount);
						var totalPages = "";
						var remainder = bidsCount % 50;
						var totalPagescount = bidsCount / 50;
						if (remainder <= 49) {
							totalPages = totalPagescount + 1;
						};
						if (pagenum == 0) {
							$('#content1').bootpag({
								total: 5,
								page: 1
							}).on("page", function(event, num) {
								$scope.getAllBids(num)
								$(this).bootpag({
									total: totalPages,
									maxVisible: 5
								});
							});
							$('#mybidscontent').bootpag({
								total: 5,
								page: 1,
							}).on("page", function(event, num) {
								$scope.getAllBids(num)
								$(this).bootpag({
									total: bidspages,
									maxVisible: 5
								});
							});
						}
						var num = [];
						var a = 0;
						for (var i = 0; i < totalPages; i++) {
							a = i + 1;
							num.push({
								"index": a
							});
						}
						$scope.number = num;
						$scope.allbids = totalBidsdata;
						$("#LoaderDiv").hide();
						var allbids = response.result.allbids;
						var bidslen = $scope.allbids.length;

						var biddername = response.uname;
						$scope.biddername = response.uname;
						$rootScope.biddername = response.uname;
						if (biddername.indexOf('.') !== -1) {
							var newName = biddername.split('.').join(" ");
						} else {
							var newName = biddername;
						}
						var finalname = eachWord(newName);
						$rootScope.name = finalname;
						$scope.name = $rootScope.name;
						var biddername = response.uname;

						var specificLength = response.specific.length;
						$scope.myBidsLen = mybidscount;
					
						$scope.specificBids = response.specific;
					} else {
						console.log(response.error);
					}
				});
			} else if ($location.search().flag == 'prev') {
				$scope.getLastSevendaysBids();
			} else {
				$scope.getTodaysBids();
			}
		}
		else
		{
          $scope.set_paging();
		}
		
	}
		//Modified By: Vimal Sharma
		//Date:15/11/2014
	$scope.showmybidspaging = function() {
		$("#content1").hide();
		$("#mybidscontent").show();
		$scope.specificBids = "";
        document.getElementById("joburl").value = "";
        document.getElementById("jobportal").value = "";
       document.getElementById("jobid").value = "";
        
       $("#status").val(-1).attr("selected", "selected");
		var srchurl=$("#srch_lead").val();
		if (srchurl=="") {

		var getbids = new GetBids();
		if ($location.search().flag == null) {
			getbids.$getAllBids(function(response) {
				if (response.status != 'false') {
					//console.log("response", response);
					var bidsCount = response.count;
					var mybidscount = response.mybidscount;
					var pagenum = 0;
					var totalBidsdata = response.result.allbids;
					if (pagenum != 0) {
						var start = (50 * pagenum) - 50 + 1;
						var end = start + 50;
						$scope.count = start;
					} else {
						$scope.count = 1;
					}
					if (mybidscount <= 50) {
						var bidspages = 1;
					} else {
						var bidspages = "";
						var remainder = mybidscount % 50;
						var pagecount = mybidscount / 50;
						if (remainder <= 49) {
							bidspages = pagecount + 1;
						};
					}
					$scope.totalBids = bidsCount;
					//console.log("bidsCount", bidsCount);
					var totalPages = "";
					var remainder = bidsCount % 50;
					var totalPagescount = bidsCount / 50;
					if (remainder <= 49) {
						totalPages = totalPagescount + 1;
					};
					if (pagenum == 0) {
						$('#content1').bootpag({
							total: 5,
							page: 1
						}).on("page", function(event, num) {
							$scope.getAllBids(num)
							$(this).bootpag({
								total: totalPages,
								maxVisible: 5
							});
						});
						$('#mybidscontent').bootpag({
							total: 5,
							page: 1,
						}).on("page", function(event, num) {
							$scope.getAllBids(num)
							$(this).bootpag({
								total: bidspages,
								maxVisible: 5
							});
						});
					}
					var num = [];
					var a = 0;
					for (var i = 0; i < totalPages; i++) {
						a = i + 1;
						num.push({
							"index": a
						});
					}
					$scope.number = num;
					$scope.allbids = totalBidsdata;
					$("#LoaderDiv").hide();
					var allbids = response.result.allbids;
					var bidslen = $scope.allbids.length;
					var biddername = response.uname;
					$scope.biddername = response.uname;
					$rootScope.biddername = response.uname;
					if (biddername.indexOf('.') !== -1) {
						var newName = biddername.split('.').join(" ");
					} else {
						var newName = biddername;
					}
					var finalname = eachWord(newName);
					$rootScope.name = finalname;
					$scope.name = $rootScope.name;
					var biddername = response.uname;
					var specificLength = response.specific.length;
					$scope.myBidsLen = mybidscount;
					
		           
					$scope.specificBids = response.specific;
				} else {
					console.log(response.error);
				}
			});
		} else if ($location.search().flag == 'prev') {
			$scope.getLastSevendaysBids();
		} else {
			$scope.getTodaysBids();
		}
	}
	else
	{
    $scope.set_paging();

	}
	
	
	}




    //Created By: Vimal Sharma
	//Date:10/03/2015
	//Bind bid details in the form on click.
    $scope.Bind_Bids_Data=function(id)
    {
    	var bidjob = new BidByJobUrl();
    	bidjob.JobUrl=id;
    	bidjob.$bidbyjoburl(function(response) {
                 	if (response.status!="false") 
                 		{
                        
                          $("#joburl").val(response.result.bid[0].JobUrl);
                          $("#name").val(response.result.bid[0].BidderName);
                          $("#jobportal").val(response.result.bid[0].JobPortal);
                          $("#jobid").val(response.result.bid[0].JobId);
                          $scope.selectedinvite=response.result.bid[0].IsInvite;
                          $scope.tech=response.result.bid[0].Technology;
                          if (response.result.bid[0].Status=="Applying") 
                           {
                           	$("#status").val(0).attr("selected", "selected");
                           }
                          if (response.result.bid[0].Status=="Applied") 
                           {
                           	$("#status").val(1).attr("selected", "selected");
                           }
                           $("#send").hide();


                 		};
                 });
        
    }

   $scope.Bids_Status=function()
    {
    	$scope.set_paging();
    	var bidjob = new BidByJobUrl();
    	bidjob.JobUrl=$("#srch_lead").val().trim();
    	bidjob.$bidbyjoburl(function(response) {
                 	if (response.status!="false") 
                 		{   
						if(response.result.bid[0]!=undefined)      {   
                        var biddername=response.result.bid[0].BidderName;
                   
                        if (response.result.bid[0].Status=="Applied") 
                           {                         	
                           	  alert("Oops! "+biddername+ " has already applied to this bid.") ;
                           }
                 		}
                 	}

                 });  
    }
    
    $scope.set_paging_withStatus=function(){
    	$scope.Bids_Status();
    	$scope.set_paging();
    }

$scope.set_paging=function()
    {
       document.getElementById("joburl").value = "";
       document.getElementById("jobportal").value = "";
       document.getElementById("jobid").value = "";
       $("#status").val(-1).attr("selected", "selected");
         var srchjoburl = new SearchJobUrl(); 
         var srjoburl=$("#srch_lead").val().trim();
         if (srjoburl!="") {
         srchjoburl.JobUrl=srjoburl;
         srchjoburl.$searchbidbyjoburl(function(response) {
                    var bidsCount = response.count;
                    if (bidsCount==0) 
                    {

                    	$("#allbidslist").css("display","inline-block");

                    } 

                    else
                    {

                    	$("#allbidslist").css("display","none");
                    }
 

					var mybidscount = response.mybidscount;
                     if (mybidscount==0) 
                    {

                    	$("#mybidslist").css("display","inline-block");
                    }  

                    else
                    {
                       $("#mybidslist").css("display","none");

                    }


					var pagenum = 0;
					var totalBidsdata = response.result.allbids;
					if (pagenum != 0) {
						var start = (50 * pagenum) - 50 + 1;
						var end = start + 50;
						$scope.count = start;
					} else {
						$scope.count = 1;
					}
					if (mybidscount <= 50) {
						var bidspages = 1;
					} else {
						var bidspages = "";
						var remainder = mybidscount % 50;
						var pagecount = mybidscount / 50;
						if (remainder <= 49) {
							bidspages = pagecount + 1;
						};
					}
					$scope.totalBids = bidsCount;
					//console.log("bidsCount", bidsCount);
					var totalPages = "";
					var remainder = bidsCount % 50;
					var totalPagescount = bidsCount / 50;
					if (remainder <= 49) {
						totalPages = totalPagescount + 1;
					};
					if (pagenum == 0) {
						$('#content1').bootpag({
							total: 1,
							page: 1
						}).on("page", function(event, num) {
							$scope.getAllBids(num)
							$(this).bootpag({
								total: totalPages,
								maxVisible: 5
							});
						});
						$('#mybidscontent').bootpag({
							total: 5,
							page: 1,
						}).on("page", function(event, num) {
							$scope.getAllBids(num)
							$(this).bootpag({
								total: bidspages,
								maxVisible: 5
							});
						});
					}
					var num = [];
					var a = 0;
					for (var i = 0; i < totalPages; i++) {
						a = i + 1;
						num.push({
							"index": a
						});
					}
					$scope.number = num;
					$scope.allbids = totalBidsdata;
					$("#LoaderDiv").hide();
					var allbids = response.result.allbids;
					var bidslen = $scope.allbids.length;
					var biddername = response.uname;
					$scope.biddername = response.uname;
					$rootScope.biddername = response.uname;
					if (biddername.indexOf('.') !== -1) {
						var newName = biddername.split('.').join(" ");
					} else {
						var newName = biddername;
					}
					var finalname = eachWord(newName);
					$rootScope.name = finalname;
					$scope.name = $rootScope.name;
					var biddername = response.uname;
					var specificLength = response.specific.length;
					$scope.myBidsLen = mybidscount;	           
					$scope.specificBids = response.specific;
         });  
         }

         else
         {
         	
         $scope.showallbidspaging(); 
          }
         //console.log("aaaaaaaaaaaaaaaaaaaaaaaaacccccccccccccccccc "+response.count);
        
                
    }















	//Modified By: Vimal Sharma
	//Date:28/7/2014
	//Edited Date:15/11/2014
	//Get all bids from database
	$scope.getAllBids = function(num) {
		$("#LoaderDiv").show();
		var pagenum = num;
		var getbids = new GetBids();

		getbids.pagenum = pagenum;
		if ($location.search().flag == null) {
			getbids.$getAllBids(function(response) {
				if (response.status != 'false') {
					//console.log("response", response);
					var bidsCount = response.count;
					var mybidscount = response.mybidscount;
					var pagenum = response.pagenum;

					var totalBidsdata = response.result.allbids;
					if (pagenum != 0) {
						var start = (50 * pagenum) - 50 + 1;
						var end = start + 50;
						$scope.count = start;
					} else {
						$scope.count = 1;
					}

					if (mybidscount <= 50) {
						var bidspages = 1;
					} else {
						var bidspages = "";
						var remainder = mybidscount % 50;
						var pagecount = mybidscount / 50;
						if (remainder <= 49) {
							bidspages = pagecount + 1;
						};
					}
					var srchlead=$("#srch_lead").val();
		            if (srchlead=="") {

				
					$scope.totalBids = bidsCount;
				       
				       }
				     else
				     {
 						
 				
					$scope.set_paging();
				    
				      }

					//console.log("bidsCount", bidsCount);
					var totalPages = "";
					var remainder = bidsCount % 50;
					var totalPagescount = bidsCount / 50;
					if (remainder <= 49) {
						totalPages = totalPagescount + 1
					};
					if (pagenum == 0) {
						if (mybidscount > 50) {
							$('#mybidscontent').bootpag({
								total: 5
							}).on("page", function(event, num) {
								$scope.getAllBids(num)
								$(this).bootpag({
									total: bidspages,
									maxVisible: 5
								});
							});
						} else {
							$('#mybidscontent').bootpag({
								total: 1
							}).on("page", function(event, num) {
								$scope.getAllBids(num)
								$(this).bootpag({
									total: bidspages,
									maxVisible: 1
								});
							});
						}
						if (bidsCount > 50) {
							$('#content1').bootpag({
								total: 5,
							}).on("page", function(event, num) {
								$scope.getAllBids(num)
								$(this).bootpag({
									total: totalPages,
									maxVisible: 5
								});
							});
						} else {
							$('#content1').bootpag({
								total: 1,
							}).on("page", function(event, num) {
								$scope.getAllBids(num)
								$(this).bootpag({
									total: totalPages,
									maxVisible: 1
								});
							});
						}
					}
					var num = [];
					var a = 0;
					for (var i = 0; i < totalPages; i++) {
						a = i + 1;
						num.push({
							"index": a
						});
					}
					$scope.number = num;
					//$scope.pages = totalPages;

					//$scope.allbids = response.result.allbids;
					$scope.allbids = totalBidsdata;
					$("#LoaderDiv").hide();
					var allbids = response.result.allbids;
					var bidslen = $scope.allbids.length;
					//$scope.totalBids = bidslen;
					var biddername = response.uname;
					$scope.biddername = response.uname;
					$rootScope.biddername = response.uname;
					if (biddername.indexOf('.') !== -1) {
						var newName = biddername.split('.').join(" ");
					} else {
						var newName = biddername;
					}
					var finalname = eachWord(newName);
					$rootScope.name = finalname;
					$scope.name = $rootScope.name;
					var biddername = response.uname;
					// var userBids = [];
					// for(var i =0; i<bidslen;i++){
					// 	if (allbids[i].BidderName == biddername){
					// 		userBids.push({"JobUrl":allbids[i].JobUrl,"JobId":allbids[i].JobId,"Status":allbids[i].Status});
					// 	}
					// }
					var specificLength = response.specific.length;
					//$scope.myBidsLen = mybidscount;
					var srchlead=$("#srch_lead").val();
		            if (srchlead=="") {

					
					$scope.myBidsLen = mybidscount;
				       
				       }
				     else
				     {
 						
 						
					$scope.set_paging();
				    
				      }
					$scope.specificBids = response.specific;
				} else {
					console.log(response.error);
				}
			});
		} else if ($location.search().flag == 'prev') {
			$scope.getLastSevendaysBids();
		} else {
			$scope.getTodaysBids();
		}
	}

	//Created By: Sachin Sharma
	//Date:29/7/2014
	//Delete a specific bids from database
	$scope.DeleteBid = function() {
		var JobId = document.getElementById('hidjob').value;
		var bidsdelete = new BidDelete();
		bidsdelete.JobId = JobId;
		bidsdelete.$deletebid(function(response) {
			if (response.status != 'false') {
				$scope.bidDeleteMsg = $rootScope.Bid_Deletion_Msg;
				hideMessage("bidDeleteMsg");
				$scope.getAllBids();
				alert("Bid Deleted Successfully");
			} else {
				alert(JSON.stringify(response));
			}

			$("#myModal").modal('hide');

		});
	}

	//Created By: Sachin Sharma
	//Date:29/7/2014
	//Change Status for specific bids in database
	$scope.ChangeStatus = function(JobId, Status, flag) {		
		var flag = flag;
		var statchange = new ChangeBidStatus();
		statchange.JobId = JobId;
		statchange.Status = Status;
		statchange.flag = flag;
		statchange.$changestatus(function(response) {
			if (response.status == 'true') {
				$scope.bidStatChangeMsg = $rootScope.Bid_StatusChange_Msg;
				hideMessage("bidStatChangeMsg");
				console.log("Status Updated successfully")
				alert($rootScope.Bid_StatusChange_Msg);
				$scope.bidstat = $rootScope.bid_change;
				if (flag == 'today') {
					$scope.getTodaysBids();
				} else if (flag == 'prev') {
					$scope.getLastSevendaysBids();
				} else {
					$scope.getAllBids();
				}
			} else {
				alert(JSON.stringify(response));
			}
		});
	}

	//Created By: Sachin Sharma
	//Date:30/7/2014
	//Get Today's bids from database
	$scope.getTodaysBids = function() {
		
		$scope.count = 1;
		var todaysbids = new TodaysBids();
		var date = new Date();
		todaysbids.today = date.toISOString();
		todaysbids.$getTodaysBids(function(response) {
			$("#LoaderDiv").show();
			if (response.status != 'false') {
				$scope.allbids = response.result.bids;
				var allbids = response.result.bids;
				var bidslen = $scope.allbids.length;
				$scope.totalBids = bidslen;
				var biddername = response.uname;
				var userBids = [];
				for (var i = 0; i < bidslen; i++) {
					if (allbids[i].BidderName == biddername) {
						userBids.push({
							"JobUrl": allbids[i].JobUrl,
							"JobId": allbids[i].JobId,
							"Status": allbids[i].Status,
							"date":allbids[i].date,
							"flag": "today"
						});
					}
				}
				var specificLength = userBids.length;
				$scope.myBidsLen = specificLength;
				$scope.specificBids = userBids;
				$location.path('/recentbids').search({
					'flag': 'today'
				});
				//console.log("$scope.specificBids--->",$scope.specificBids);
			} else {
				console.log(response.error);
			}
			$("#LoaderDiv").hide();
		});
		
	}

	//Created By: Sachin Sharma
	//Date:30/7/2014
	//Get Previous Seven Days bids from database
	$scope.getLastSevendaysBids = function() {
		
		$scope.count = 1;
		var prevbids = new PrevSevenDaysBids();
		prevbids.$getPrevSevenDaysBids(function(response) {
			$("#LoaderDiv").show();
			if (response.status != 'false') {
				$scope.allbids = response.result.bids;
				var allbids = response.result.bids;
				var bidslen = $scope.allbids.length;
				$scope.totalBids = bidslen;
				$scope.biddername = response.uname;
				var biddername = response.uname;
				var userBids = [];
				for (var i = 0; i < bidslen; i++) {
					if (allbids[i].BidderName == biddername) {
						userBids.push({
							"JobUrl": allbids[i].JobUrl,
							"JobId": allbids[i].JobId,
							"Status": allbids[i].Status,
							"date":allbids[i].date,
							"flag": "prev"
						});
					}
				}
				var specificLength = userBids.length;
				$scope.myBidsLen = specificLength;
				$scope.specificBids = userBids;
				$location.path('/recentbids').search({
					'flag': 'prev'
				});
			} else {
				console.log(response.error);
			}
			$("#LoaderDiv").hide();
		});
		
	}

	//Created By: Sachin Sharma
	//Date:06/08/2014
	//Show bootstrap modal for tasks
	$scope.tasksModal = function(even, url, id, date) {
		//console.log(even.clientX);
		console.log(even.clientY);
		var yaxix=even.clientY-140;
		console.log(yaxix);		
		var url = url;
		var JobId = id;
		var date1 = new Date(date);
		
		//$("#showclock").attr("title", date1.toDateString());
		$scope.hidjob = id;
		$scope.hidurl = url;
		$('#myModal').modal('toggle');
		$("#myModal").css('top',yaxix);
	}


	$scope.hidetasksModal = function() {	
		$('#myModal').modal('hide')  ;
	}

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Show bootstrap modal to convert to lead
	$scope.lead = function() {


     
      
      var JOBTITLE="";
      var url = document.getElementById('hidurl').value; 

      if(url.indexOf("odesk")>0)
      {

      var urlindex=url.indexOf("_");
      var urlsubstring= url.substring(27, urlindex);
      JOBTITLE=urlsubstring.replace(/[^a-zA-Z ]/g, " ").trim();
   
      }

       if(url.indexOf("elance")>0)
      {

      var urlindex=url.indexOf("/?");
      var urlsubstring= url.substring(25, urlindex);
      JOBTITLE=urlsubstring.replace(/[^a-zA-Z ]/g, " ").trim();
      
   
      }

      var absUrl = $location.absUrl();

      var pos = absUrl.lastIndexOf("recentbids");

      absUrl= absUrl.substring(0, pos);
      
      //$window.location.href = absUrl+'recentleads?job_title='+JOBTITLE;
      $location.path('/recentleads').search({'job_title': "test"});
      

     

	 
	 }


	 $scope.remove_appurl_validation=function()
	 {
        $("#appurl").css("border-color", ""); 
	 }

	 $scope.remove_title_validation=function()
	 {
	 	$("#title").css("border-color", "");
	 }

	 $scope.remove_interview_validation=function()
	 {
	 	$("#interview").css("border-color", "");
	 }

	 $scope.remove_bidamount_validation=function()
	 {
	 	$("#bidamount").css("border-color", "");
	 }

	 $scope.remove_cname_validation=function()
	 {
	 	 $("#cname").css("border-color", "");
	 }

	 $scope.remove_budget_validation=function()
	 {
	 	$("#budget").css("border-color", "");
	 }

	 $scope.remove_clienthistory_validation=function()
	 {
	 	$("#clienthistory").css("border-color", "");
	 }

	 $scope.remove_leadtype_validation=function()
	 {
	 	$("#leadtype").css("border-color", "");
   var ldtype=$scope.leadtype;
  
   if(ldtype=="Hourly")
   {
    $("#SpanClientBudget").text("");
           $("#budget").css("border-color", "");
   }
   else
   {
    $("#SpanClientBudget").text("*");
   }
	 }

	 $scope.remove_statuslead_validation=function()
	 {
	 	$("#statuslead").css("border-color", "");
	 }

	  $scope.remove_clientemail_validation=function()
	 {
	 	$("#clientemail").css("border-color", "");
	 }

	   $scope.remove_msgurl_validation=function()
	 {
	 	$("#msgurl").css("border-color", "");
	 }

	//Created By: Sachin Sharma
	//Date:31/7/2014
	//Convert Bid To Lead
	$scope.ConvertToLead = function(joburl, jobid) {
		var url = joburl;
		var lead = new Lead();
		var client = new Client();
		var bidchange = new ChangeBidType();
        var clientlead = new ClientLead();

		bidchange.JobId = jobid;
		//data to be saved in lead collection
		lead.JobId = jobid;
		lead.JobTitle = $scope.jobtitle;
		lead.ApplicationUrl = $("#appurl").val();
		lead.MessageUrl = $("#msgurl").val();
		lead.Interviews = $scope.interviews;
		lead.BidAmount = $scope.amount;
		lead.LeadType = $scope.leadtype;
		lead.CurrentStatus = $scope.status;
		//alert(lead.LeadType);
		lead.date = new Date();
		//data to be saved in client collection
		client.JobId = jobid;
		client.ClientName = $scope.clientname;
		client.ClientEmail = $("#clientemail").val();
		client.ClientBudget = $scope.clientbudget;
		client.ClientSkype = $scope.clientskype;
		client.ClientHistory = $scope.history;
		client.date = new Date();


        clientlead.JobId = jobid;
        clientlead.JobTitle = $scope.jobtitle;
        clientlead.ApplicationUrl = $("#appurl").val();
        clientlead.MessageUrl = $("#msgurl").val();
        clientlead.Interviews = $scope.interviews;
        clientlead.BidAmount = $scope.amount;
        clientlead.LeadType = $scope.leadtype;
        clientlead.CurrentStatus = $scope.status;
        clientlead.ClientName = $scope.clientname;
        clientlead.ClientEmail = $("#clientemail").val();
        clientlead.ClientBudget = $scope.clientbudget;
        clientlead.ClientSkype = $scope.clientskype;
        clientlead.ClientHistory = $scope.history;



     
      $("#reqFieldsMsg").hide();


      
		if(lead.JobTitle !="" && typeof lead.JobTitle !="undefined"  && lead.ApplicationUrl != "" && lead.Interviews != "" && typeof lead.Interviews !="undefined" && lead.BidAmount !="" && typeof lead.BidAmount !="undefined" && lead.LeadType !="" && typeof lead.LeadType !="undefined" && lead.CurrentStatus !="" && typeof lead.CurrentStatus !="undefined" && client.ClientName!="" && typeof client.ClientName !="undefined" && client.ClientHistory !="" && typeof client.ClientHistory !="undefined") {
		var regurl = new RegExp(/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/);
	 		//console.log(lead.MessageUrl , regurl.test(lead.MessageUrl)  , typeof lead.MessageUrl );
			if(regurl.test(lead.ApplicationUrl) == true){
		 		var regmail = new RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/);
				if(client.ClientEmail == "" || typeof  client.ClientEmail == "undefined"  || regmail.test(client.ClientEmail) == true){             
			

			
	if(regurl.test(lead.MessageUrl) == false && lead.MessageUrl != ""){

	alert("Please enter valid url in Message Url.");
	$("#msgurl").css("border-color", "red");
        return false;
};

	if (check_field(lead.Interviews)==false) {
       $("#interview").css("border-color", "red");
        alert("Please enter numeric in No. of Interviews.");
        return false;
      };

        if (lead.LeadType=="Fixed") {

            if (client.ClientBudget=="" || typeof client.ClientBudget=="undefined") {
            	
                alert("Please enter Client Budget.");
				$("#budget").css("border-color", "red");
				return false;
			}
		};


		

     if(typeof client.ClientBudget !="undefined")
     {
      if (check_field(client.ClientBudget)==false) {

      	
       $("#budget").css("border-color", "red");     
        alert("Please enter numeric in Client Budget.");
        return false;
      }};  

       if (check_field(lead.BidAmount)==false) {
       $("#bidamount").css("border-color", "red");
        alert("Please enter numeric in Bid Amount.");
        return false;
      };  
           
         
          
           if ((lead.LeadType=="Fixed" && client.ClientBudget!="") || (lead.LeadType=="Hourly" && (client.ClientBudget =="" || typeof client.ClientBudget=="undefined") ) || (lead.LeadType=="Hourly" && (client.ClientBudget !="")) )
           	{

           		
					lead.$saveLead(function(response) {
					console.log(response);
					if (response.status == 'true') {
						console.log("Lead Added successfully");
					} else {
						console.log("error", response.error);
					}
				});

				client.$saveClient(function(response) {
					console.log(response);
					if (response.status == 'true') {
						console.log("Client Added successfully");
					} else {
						console.log("error", response.error);
					}
				});

				// clientlead.$saveclientlead(function(response) {
				// 	console.log(response);
				// 	if (response.status == 'true') {
				// 		console.log("Client Lead Added successfully");
				// 	} else {
				// 		console.log("error", response.error);
				// 	}
				// });
				
				bidchange.$changeToLead(function(response) {
					if (response.status == 'true') {
						console.log("Bid Type Changed successfully");
					} else {
						console.log("error", response.error);
					}
				});

				$('#myModal1').modal('hide');
				$scope.bidtoLeadMsg = $rootScope.Bid_ToLead_Msg;
				alert($rootScope.Bid_ToLead_Msg);
				hideMessage("bidtoLeadMsg");
				$scope.getAllBids();
}
else
{

	// alert("testtttttttttttttttttttttt");

 //      $("#budget").css("border-color", "red");
 //        alert("Please enter Client Budget.");
 //        return false;

}


		} else{
			$scope.emailMsg = $rootScope.invalid_Email_Message;
			alert($scope.emailMsg);
			$("#clientemail").css("border-color", "red");
			return false;
		}

		} else{
			$scope.urlMsg = $rootScope.invalid_Url_Message;
			alert($scope.urlMsg); 
			$("#appurl").css("border-color", "red");
			return false;
		}

		} else {           
            if (lead.ApplicationUrl=="" && lead.JobTitle=="" && lead.Interviews=="" && lead.BidAmount=="" && client.ClientName=="" && client.ClientBudget=="" && client.ClientHistory=="" && lead.CurrentStatus=="" && (lead.LeadType=="" || typeof lead.LeadType=="undefined") ) {

                $scope.reqFieldsMsg = $rootScope.required_Fields_Msg;
                $("#appurl").css("border-color", "red");
				$("#title").css("border-color", "red");
				$("#interview").css("border-color", "red");
				$("#bidamount").css("border-color", "red");
				$("#cname").css("border-color", "red");
				//$("#budget").css("border-color", "red");
				$("#clienthistory").css("border-color", "red");
				$("#leadtype").css("border-color", "red");
				$("#statuslead").css("border-color", "red");
				showMessage("reqFieldsMsg");
			    hideMessage("reqFieldsMsg");


            	return false;
            };

         
            if (lead.JobTitle=="" ||typeof lead.JobTitle=="undefined") {

                alert("Please enter Job Title.")
				$("#title").css("border-color", "red");
				return false;
			};

			if (lead.ApplicationUrl=="" || typeof lead.ApplicationUrl=="undefined") {

				alert("Please enter Application Url.")
				$("#appurl").css("border-color", "red");
				return false;

			};

			

		    if (lead.Interviews=="" || typeof lead.Interviews=="undefined" ) {

                alert("Please enter Number of Interviews.")
				$("#interview").css("border-color", "red");
				return false;
			};

			if (lead.BidAmount=="" || typeof lead.BidAmount=="undefined" ) {

				alert("Please enter Bid Amount.")

				$("#bidamount").css("border-color", "red");
				return false;
			}; 
			if (client.ClientName=="" || typeof client.ClientName=="undefined") {

                alert("Please enter Client Name.");
				$("#cname").css("border-color", "red");
				return false;
			};

			

            if (lead.LeadType=="Fixed") {

            if (client.ClientBudget=="" || typeof client.ClientBudget=="undefined") {
            	
                alert("Please enter Client Budget.");
				$("#budget").css("border-color", "red");
				return false;
			}
		};

			 if (client.ClientHistory=="" || typeof client.ClientHistory=="undefined") {
            	
                 alert("Please enter Client History.");
				$("#clienthistory").css("border-color", "red");
				return false;
			};

			 if (lead.LeadType=="" || typeof lead.LeadType=="undefined") {
            	
                 alert("Please select Lead Type.");
				$("#leadtype").css("border-color", "red");
				return false;
			};
			if (lead.CurrentStatus=="" || typeof lead.CurrentStatus=="undefined") {
            	
                 alert("Please enter Current Status.")
				$("#statuslead").css("border-color", "red");
				return false;
			};
			
		}
	}
});

//Created By: Sachin Sharma
//Date:29/7/2014
// function to validate Url 
function validateUrl(url) {
	var url = url;
	var rurl = /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;
	if ((rurl.test(url))) {
		var res = true;
	} else {
		var res = false;
	}
	return res;
}

//Created By: Sachin Sharma
//Date:28/7/2014
// function to Get JobId from JobUrl 
function getid(value) {
		var tilt = value.split('~');
		var splitslash = tilt[0].split('/');
		var test = (splitslash[2])
		if (test == "www.odesk.com") {
			var check = value.split('/');
			var len = check.length;
			var last = (check[len - 1]);
			var slast = (check[len - 2]);
			if (slast == "applications") {
				return false;
			}
			var JobId = value.split('~');
			var id = JobId[1]
			var check1 = (id.indexOf('?') > -1); //true
			var check2 = (id.indexOf('/') > -1); //true
			if (check1) {
				var Id = id.split('?');
				var JobId = Id[0];
			}
			if (check2) {
				var Id = id.split('/');
				var JobId = Id[0];
			} else {
				var JobId = JobId[1];
			}
		} else if (test == "www.elance.com") {
			var regex = /\/\d+\//;
			var url = value;
			var exist = url.match(regex);
			var strid = String(exist);
			var JobId = strid.replace(/\//g, '');
			JobId = JobId;
		}
		return JobId;
	}
	//ends here

//Created By: Sachin Sharma
//Date:04/08/2014
// function to hide the messages after 4 seconds 
function hideMessage(id) {	
	setTimeout(function() {
		$('#' + id).hide();
	}, 2000);
}

function showMessage(id) {
	setTimeout(function() {
		$('#' + id).show();
	});
}

function eachWord(str) {
	return str.replace(/\w\S*/g, function(txt) {
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
}
function check_field(id) {    
                if (isNaN(id)) {
                    return false;
                }
            }
