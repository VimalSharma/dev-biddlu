var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Project = mongoose.model('Project');
var db = mongoose.connection;
var ProjectKeyword = mongoose.model('ProjectKeyword');
var Lead = mongoose.model('Lead');
var client = mongoose.model('Client');
var myModel = mongoose.model('projkey_name_join');

// Created By: Parveen Yadav
// Date: 05/07/2014
// Puropse:TO get login Details on basis of UserName and Password paramteres
exports.login = function(req, res) {
  User.findOne({
    UserName: req.body.UserName,
    Password: req.body.Password
  }, function(err, the_user) {
    if (!err && the_user != null) {
      req.session.user = the_user.id;
      req.session.login = 1;
      req.session.username = req.body.UserName;
      res.json({
        "status": "true",
        "users": the_user
      });
     // ClientLeadJoin();
      // ProjectKeywordIdJoin(function(){
      //              projkeynamejoin(function(err,res){
      //               projdevloperjoin(function(err,res){                  
      //              });             
      //              });
      //            });
    }
    res.send({
      "status": "false",
      "error": err
    });
  })
};

// Created By: Parveen Yadav
// Date: 16/07/2014
// Puropse:empty the user's session to log them out 

exports.logout = function(req, res) {
  req.session.user = "";
  req.session.login = 0;
  res.send({
    "status": "true"
  });
};

// Created By: Parveen Yadav
// Date: 24/07/2014
// Puropse:To check user is currently logged in or not 

exports.isAuthorised = function(req, res) {
  if (req.session.login == 1) {
    res.send({
      "status": "true"
    });
  } else {
    res.send({
      "status": "false"
    });
  }
};

// Created By: Vimal Sharma
// Date: 31/10/2014
// Puropse:To create collection which contains Client and Lead data. 
function ClientLeadJoin() {
    var o = {};
    o.map = function() {
      if (this.JobId != null) {
        print(this.JobId);
        var output = {
          _id: this._id,
          ApplicationUrl: this.ApplicationUrl,
          JobTitle: this.JobTitle,
          JobId: this.JobId,
          MessageUrl: this.MessageUrl,
          Interviews: this.Interviews,
          CurrentStatus: this.CurrentStatus,
          LeadType: this.LeadType,
          BidAmount: this.BidAmount,
          ClientName: db.clients.findOne({
            "JobId": this.JobId
          }).ClientName,
          ClientEmail: db.clients.findOne({
            "JobId": this.JobId
          }).ClientEmail,
          ClientBudget: db.clients.findOne({
            "JobId": this.JobId
          }).ClientBudget,
          ClientSkype: db.clients.findOne({
            "JobId": this.JobId
          }).ClientSkype,
          ClientHistory: db.clients.findOne({
            "JobId": this.JobId
          }).ClientHistory
        }
        emit("", output);
      }
    }
    o.reduce = function(key, values) {
      var outs = {
        _id: null,
        ApplicationUrl: null,
        JobTitle: null,
        MessageUrl: null,
        Interviews: null,
        CurrentStatus: null,
        LeadType: null,
        BidAmount: null,
        ClientName: null
      }
      values.forEach(function(v) {
        if (outs._id != null) {
          if (outs.ApplicationUrl == null) {
            outs.ApplicationUrl = v.ApplicationUrl
          }
          if (outs.JobTitle == null) {
            outs.JobTitle = v.JobTitle
          }
          if (outs.MessageUrl == null) {
            outs.MessageUrl = v.MessageUrl
          }
          if (outs.Interviews == null) {
            outs.Interviews = v.Interviews
          }
          if (outs.CurrentStatus == null) {
            outs.CurrentStatus = v.CurrentStatus
          }
          if (outs.LeadType == null) {
            outs.LeadType = v.LeadType
          }
          if (outs.BidAmount == null) {
            outs.BidAmount = v.BidAmount
          }
          if (outs.ClientName == null) {
            outs.ClientName = v.ClientName
          }
        }
      });
      return outs;
    }
    o.out = {
      replace: 'client_lead_joins'
    }
    o.verbose = true;
    Lead.mapReduce(o, function(err, results) {
      console.log(err)
    })
  }

  // Created By: Vimal Sharma
  // Date: 31/10/2014
  // Puropse:To create collection which contains ProjectId,KeywordId and Keywordname. 
function ProjectKeywordIdJoin(callback) {
  var o = {};
  o.map = function() {
    if (typeof this.developerId !== 'undefined') {
      print("Hello");
    } else if (this.keywordId.length === 24) {
      var output = {
        projectId: this.projectId,
        keywordId: this.keywordId,
        keywordname: db.keywords.findOne({
          _id: ObjectId(this.keywordId)
        }).text
      }
      emit(this._id, output);
    }
  }
  o.reduce = function(key, values) {
    var outs = {
      projectId: null,
      keywordId: null,
      keywordname: null
    }
    values.forEach(function(v) {
      if (outs.projectId != null) {
        if (outs.projectId == null) {
          outs.projectId = v.projectId
        }
        if (outs.keywordId == null) {
          outs.keywordId = v.keywordId
        }
        if (outs.keywordname == null) {
          outs.keywordname = v.keywordname
        }
      }
    });
    return outs;
  }
  o.out = {
    replace: 'proj_key_id_joins'
  }
  o.verbose = true;
  ProjectKeyword.mapReduce(o, function(err, results) {
    console.log(err)
    callback();
  })
}

// Created By: Vimal Sharma
// Date: 31/10/2014
// Puropse:To create collection which contains Project Details with keywordname. 
function projkeynamejoin(callback) {
  var o = {};
  o.map = function() {
    for (var i = 0; i < this.KeywordsId.length; i++) {
      if (this.KeywordsId[i].length === 24) {
        var output = {
          Title: this.Title,
          Url: this.Url,
          Description: this.Description,
          DevelopersId: this.DevelopersId,
          keywordname: db.proj_key_id_joins.findOne({
            "value.projectId": this._id.valueOf(),
            "value.keywordId": this.KeywordsId[i]
          }).value.keywordname
        }
        emit(this._id, output);
      }
    }
  }
  o.reduce = function(key, values) {
    var outs = {
      Title: null,
      Url: null,
      DevelopersId: null,
      Developers: null,
      keywordname: null
    }
    values.forEach(function(v) {
      if (outs.Title == null) {
        outs.Title = v.Title;
      }
      if (outs.Url == null) {
        outs.Url = v.Url
      }
      if (outs.Description == null) {
        outs.Description = v.Description
      }
      if (outs.DevelopersId == null) {

        outs.DevelopersId = v.DevelopersId;
      }
      if (outs.keywordname == null) {
        var all = [];
        values.forEach(function(x) {
          all.push(x.keywordname)
        });
        outs.keywordname = all;
      }
    });
    return outs;
  }
  o.out = {
    replace: 'projkey_name_joins'
  }
  o.verbose = true;
  Project.mapReduce(o, function(err, results) {
    console.log(err)
    callback();
  })
}

// Created By: Vimal Sharma
// Date: 31/10/2014
// Puropse:To create collection which contains Project Details with Developer name. 
function projdevloperjoin(callback) {
  var o = {};
  o.map = function() {
    if (this.value.DevelopersId.length > 0) {
      var projectId = this._id.valueOf();
      print(this._id.valueOf());
      for (var i = 0; i < this.value.DevelopersId.length; i++) {

        var output = {
          projectId: this._id.valueOf(),
          Title: this.value.Title,
          Url: this.value.Url,
          Description: this.value.Description,
          Developers: db.developers.findOne({
            "_id": ObjectId(this.value.DevelopersId[i])
          }).text,
          keywordname: this.value.keywordname
        }
        emit(this._id, output);

      }
    }
  }
  o.reduce = function(key, values) {
    var outs = {
      projectId: null,
      Title: null,
      Url: null,
      Description: null,
      Developers: null,
      keywordname: null
    }
    values.forEach(function(v) {

      if (outs.projectId == null) {
        outs.projectId = v.projectId;
      }
      if (outs.Title == null) {
        outs.Title = v.Title;
      }
      if (outs.Url == null) {
        outs.Url = v.Url
      }
      if (outs.Description == null) {
        outs.Description = v.Description
      }
      if (outs.Developers == null) {
        var all = [];
        values.forEach(function(x) {
          all.push(x.Developers)
        });
        outs.Developers = all;
      }
      if (outs.keywordname == null) {
        outs.keywordname = v.keywordname;
      }
    });
    return outs;
  }
  o.out = {
    replace: 'searches'
  }
  o.verbose = true;
  myModel.mapReduce(o, function(err, results) {
    console.log(err)
    callback();
  })
}