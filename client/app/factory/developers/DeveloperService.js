 // Factory to hit All Keyword related RESTFUL API===========================*****//
DeveloperService.factory("Developer",   function($resource) {  // Developers API*****//
    return $resource('/api/developers/:id', {
      id: '@id'
      }, {
        developerautocomplete: {
          method:"GET",
          isArray:false
        },
    });
});

DeveloperService.factory("GetDeveloper",   function($resource) {  // Developers API*****//
    return $resource('/api/getdevelopers/:id', {
      id: '@id'
      }, {
        getDeveloperBydevid: {
          method:"POST",
          isArray:false
        },
    });
});