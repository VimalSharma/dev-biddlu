var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Developer = new Schema({
  text: String,
  CreatedDate: String,
  ModifyDate: String,
  Deleted: Boolean,
  IsActive: Boolean
 
}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

mongoose.model('Developer', Developer);
