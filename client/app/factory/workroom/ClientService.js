/*jslint node: true */
/*global angular */
"use strict";



 // Factory to hit All Clients related RESTFUL API===========================*****//

ClientService.factory("Client",   function($resource) {

    return $resource('/api/clients/:id', {  // Client API*****//
      id: '@id'
      }, {
          saveClient: {  
          method:"POST"
        },
    });
})

ClientService.factory("EditClient",   function($resource) {

    return $resource('/api/editclients/:JobId', {  // Client API*****//
      JobId: '@JobId'
      }, {
          getEditClient: {  
          method:"POST"
        },
    });
})

ClientService.factory("ClientUpdate",   function($resource) {

    return $resource('/api/clientsupdate/:JobId', {  // Lead API*****//
      JobId: '@JobId'
      }, {
          updateClient: {  
          method:"POST"
        }
    });
})