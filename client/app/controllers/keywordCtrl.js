/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";



angular.module("myApp.controllers").controller("keywordCtrl", function($scope, Keyword,$http) {
       
// //TO get all the Keywords=================================================================
       $scope.GetKeywords = function(query) {
    return $http.get('api/keywords?key='+query);
  }; 


});