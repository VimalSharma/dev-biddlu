
var mongoose = require('mongoose'), Model = mongoose.model('Keyword');
mongoose.set('debug', true);


      // Created By: Parveen Yadav
      // Date: 15/07/2014
      // Puropse: TO search Keywords
		
			exports.keyautocomplete = function(req, res) {
			  Model.find({text: new RegExp(req.query.key, 'i')}, function(err, the_keywords){
			    if(the_keywords.length > 0){
			    	res.send(the_keywords);
			    }else{
        var resp=[];
        console.log(resp);
        res.send(resp);
              
			    }
			  });
			}


        // Created By: Parveen Yadav
        // Date: 15/07/2014
        // Puropse: Get  Keyword  list by their keyword Id
      
         exports.getKeywordsByKeyid = function(req, res) {

          
          Model.findById(req.params.id, function(err, keywords) {
        
            if((err==null ) && (keywords!=null)){
                var json={};
                json={"keywordList":keywords}
                console.log(JSON.stringify(keywords));
                 res.send({"status":"true"},json); 
            }
            else
            {
             res.send({"status":"false","error":err}); 
            }
          });
        };


          // Created By: 53e366acec785106efe50bc8
          // Date: 04/08/2014
          // Puropse: Save Keywordm in database
         
        exports.saveKeywords = function(req, res) {
               
               var document = new Model(req.body);
               document.save(function(err, keywords) {
                   if (err) {
                    res.send({"status":"false","error":err}); 
                  }
                    res.send({"status":"true","keywords":keywords});
                });
            
        };

        // Created By: 53e366acec785106efe50bc8
        // Date: 07/08/2014
        // Puropse: Get Keywordm by Keywords Name
       exports.getKeywordsByKeyName = function(req, res) {
        
        Model.find({text: req.body.text}, function(err, keywords) {
      
          if((err==null ) && (keywords!=null)){
              var json={};
              json={"keywordList":keywords}
              console.log(JSON.stringify(json));
               res.send({"status":"true"},json); 
          }
          else
          {
           res.send({"status":"false","error":err}); 
          }
        });
        
      };
