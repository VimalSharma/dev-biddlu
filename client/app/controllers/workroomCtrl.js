/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";


//Project Controller starts here======================================**//
angular.module("myApp.controllers").controller("workroomCtrl", function ($scope, Workroom,WorkroomGetJob,$route,$rootScope, $location) {

    var workroom = new Workroom();
    var workroomgetjob = new WorkroomGetJob();

    //Intilize function to Add a New Job==============================**//
    $scope.AddJob = function (url,bidType) {
        workroom.url = url;
        workroom.bidType = bidType;
    
        workroom.$addJob(function (response) {
            if (response.status != 'false') {
              alert("Successful");
              $route.reload();
            } else {
                alert(response);
            }
        });
    };
    //*******Project function end here*******//


  
   //Intilize function to Read a  Job==============================**//
    $scope.ReadJob = function (url,bidType) {
 
        var workroomgetjob = new WorkroomGetJob();
        workroomgetjob.url = url;
        workroomgetjob.$getJobByUrl(function (response) {
            console.log("respons------>e",response.result.workroom);
            if (response.result.workroom != '') {

              alert("Jobs allready Exist");
              
            } else {
                $scope.AddJob(url,bidType);
              //alert("error");
                  
            }
        });
    };
    //*******ReadJob function end here*******//


  //Intilize function to Read a  Job==============================**//
    $scope.ReadAllJob = function () {
           
        workroom.$getAllJobs(function (response) {
            if (response.status != 'false') {
             // alert("Successful"+"Read job")
             $scope.alljobs=response;
             console.log(JSON.stringify(response));
              
            } else {
              //alert("error");
                  
            }
        });
    };
    //*******ReadJob function end here*******//


      //Delete Query  Jobs====================================**//
    $scope.DeleteWorkroom = function (job_id, job_data) {
        var workroom = new Workroom();
        workroom.id = job_id

        workroom.$removeJobs(function (response) {

            if (response.status != 'false') {
                console.log("sdasf",$scope.alljobs)
                var idx = $scope.alljobs.workroom.indexOf(job_data);
                if (idx >= 0) {
                    $scope.alljobs.workroom.splice(idx, 1);
                }
            } else {
                alert(JSON.stringify(response));
            }
        });

    }; //*******Delete jobs function end here*******//












}); //*******Controller Scope  end here*******//