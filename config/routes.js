var user = require('../server/service/user');
var project = require('../server/service/project');
var keyword = require('../server/service/keyword');
var projectkeyword = require('../server/service/projectkeyword');

var workroom = require('../server/service/workroom');
var developer = require('../server/service/Developer');
var bid = require('../server/service/bids');
var lead = require('../server/service/lead');
var client = require('../server/service/client');

module.exports = function(app){
  app.post('/users', user.login);
  app.get('/users', user.logout);
  app.get('/usersisauthorised', user.isAuthorised);


  //project Routes here
  app.post('/projects', project.addProject);
  app.get('/projects', project.projectList);
  app.post('/projectslists/:id', project.projectListByProjectId);
  app.del('/projects/:id', project.removeProject);
  app.post('/projects/:id', project.editProject);
  app.put('/projects/:id', project.updateProject);

  //projectkeywords routes here
  app.del('/deletekeywords/:projectId', projectkeyword.deletekeywordsIdByProjectId);
  app.post('/projectkeywords', projectkeyword.addKeywords);
  app.post('/getkeyidbypid', projectkeyword.getKeywordsIdByPid);
  app.post('/projectgetkeywords', projectkeyword.getprojectsIdByKeyid);

   // app.post('/workrooms', workroom.addJob);
   // app.post('/workroomsgetjob', workroom.getJobByUrl);
   // app.get('/workrooms', workroom.getAllJobs);
   // app.del('/workrooms/:id', workroom.removeJobs);

  app.post('/savedevelopers', projectkeyword.addDevelopers);
 


  //Keywords route here
  app.get('/keywords', keyword.keyautocomplete);
  app.post('/keywords/:id', keyword.getKeywordsByKeyid);
  app.post('/savekeywords', keyword.saveKeywords);
  app.post('/getkeywordsbykey/:text', keyword.getKeywordsByKeyName);

   app.post('/workrooms', workroom.addJob);
   app.post('/workroomsgetjob', workroom.getJobByUrl);
   app.get('/workrooms', workroom.getAllJobs);
   app.del('/workrooms/:id', workroom.removeJobs);

  app.get('/developers', developer.developerautocomplete);
  app.post('/getdevelopers/:id', developer.getDeveloperBydevid);
  
  app.post('/bids', bid.savebid);
  app.post('/ClientLead', bid.saveclientlead);
  app.post('/getbids',bid.getAllBids);
  app.del('/bidsdelete/:JobId',bid.deletebid);
  app.post('/duplicatebid/:JobId',bid.duplicatebid);
  app.post('/bidbyjoburl/:JobUrl',bid.bidbyjoburl);
  app.post('/searchbyjoburl/:JobUrl',bid.searchbidbyjoburl);
  app.post('/changebidstatus/:JobId',bid.changestatus);
  app.post('/changebidstype/:JobId',bid.changeToLead);
  app.post('/todaysbids',bid.getTodaysBids);
  app.post('/prevsevendaysbids',bid.getPrevSevenDaysBids);
  app.post('/leads', lead.saveLead);
  app.post('/clients', client.saveClient);
  app.post('/getleads',lead.getAllLeads);
  app.post('/todaysleads',lead.getTodaysLeads);
  app.post('/prevsevendaysleads',lead.getPrevSevenDaysLeads);
  app.del('/leadsdelete/:JobId',lead.deleteLead);
  app.post('/editleads/:JobId',lead.getEditLead);
  app.post('/leadsupdate/:JobId',lead.updateLead);
  app.post('/editclients/:JobId',client.getEditClient);
  app.post('/clientsupdate/:JobId',client.updateClient);
};