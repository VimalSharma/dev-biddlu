var mongoose = require('mongoose'), Project = mongoose.model('Project'),projectkeywordModel=mongoose.model('ProjectKeyword'),keywordModel=mongoose.model('Keyword'),developerModel=mongoose.model('Developer'),Search = mongoose.model('Search');
var myModel = mongoose.model('projkey_name_join');
// var db = mongoose.connection;
mongoose.set('debug', true);



    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: Save project data in mongoose database
   
  exports.addProject = function(req, res) {
    
         var document = new Project(req.body);
         document.save(function(err, project) {
             if (err) {
              res.send({"status":"false","error":err}); 
            }
              res.send({"status":"true"},project);
             
          });
      
  };



    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: Find Query Get List of Projects
 
    exports.projectList = function(req, res) {
    Project.find({}, null, {
      sort: {
        '_id': -1
      }
    }, function(err, project_list) {   //monoose query to find all data
                var json={};
              if((err==null ) && (project_list!=null)){
                json={"project":project_list}
                  res.send({"status":"true"},json);  
                 }else{
                res.send({"status":"false","error":err}); 
              }
            });

  };

 
     // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse: Delete  Projects

    exports.removeProject = function(req, res) {
      Project.findByIdAndRemove(req.params.id, function(err, projects) {
        if (err) {
           throw new Error({"status":"false","error":err});
        }
        Search.findByIdAndRemove(req.params.id, function(err, projects) {
        if (err) {
          throw new Error({"status":"false","error":err});
        }
        myModel.findByIdAndRemove(req.params.id, function(err, projects) {
        if (err) {
          throw new Error({"status":"false","error":err});
        }
        });
      });
        res.send({"status":"true"},projects);
      });
    };



     // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse:Get  project List by Id

       exports.editProject = function(req, res) {
        Project.findById(req.params.id, function(err, project) {
          var json={};
          if((err==null ) && (project!=null)){
             json={"project":project}
             console.log("????????????",json);
            res.send({"status":"true"},json);  
          }
          else
          {
            console.log("????????????11111",err);
           res.send({"status":"false","error":err}); 
          }
        });
      };

   

    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse:Update Project according to Id
  
      exports.updateProject = function(req, res) {
      Project.findByIdAndUpdate(req.params.id, {
        $set: req.body
      }, function(err, project) {
        if (err) {
          res.send({"status":"false","error":err});
        }
        res.send({"status":"true"},project);
      });
    };  




    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse:Get all project List by their project ID
   // var collectProjectId=[];
    
    exports.projectListByProjectId = function(req, res) {
      
      var keywordsArray=[];
      var json={};
      var keywords={};
      var developers={};
           
      
                 

                        Project.find({_id: req.body.id}, function(err, project_list){ 

                          if(project_list.length>0){
                         
                         
                             //Get all Keywords aacording to response in Project_List keyworsdsId
                              GetKeywords(project_list[0].KeywordsId,function(keywordResult){
                                    
                                    keywords={"Keywords":keywordResult};

                                    //push keywords name into ProjectLsit and move forward to add developers in same Array
                                    project_list.push(keywords);

                                    //Get all Developers aacording to response in Project_List developersId

                                    GetDevelopers(project_list[0].DevelopersId,function(developerResult){
                                  
                                    developers={"Developers":developerResult};
                                     
                                     //push developers name into ProjectLsit and return to controller as single Array Object 
                                    project_list.push(developers);


                                    //make array a  json object and send it to controller
                                          
                                        json={"project":project_list}

                                        res.send({"status":"true"},json); 

                                    });


                                }); 


                             }else{

                            res.send({"status":"false","error":err}); 

                          }
                    });
    };


   
    // Created By: Parveen Yadav
    // Date: 29/07/2014
    // Puropse:Callback function to get Keywords

   function GetKeywords( id ,callback) {
                          
                       var array=[]; 
                        var j=0;
                        for(var i=0;i<id.length;i++)
                         { 

                           keywordModel.find({_id:id[i]},function(err,keywords){
                           if(typeof keywords!=="undefined")
                              {
                            array.push(keywords[0].text);
                          }
                            j=j+1;

                                if(j==id.length)
                                 {
                              
                                   callback(array);

                                 }

                            });
                          
                         }
                            
                  }




    // Created By: Parveen Yadav
    // Date: 30/07/2014
    // Puropse:Callback function to get Developers

   function GetDevelopers( id ,callback) {
                          
                       var array=[]; 
                        var j=0;
                        for(var i=0;i<id.length;i++)
                         { 

                           developerModel.find({_id:id[i]},function(err,developers){
                           
                              console.log("dsfsdfsdfsdfds???",developers);
                              if(typeof developers!=="undefined")
                              {
                            array.push(developers[0].text)
                     }
                            j=j+1;

                                if(j==id.length)
                                 {
                              
                                   callback(array);

                                 }

                            });
                          
                         }
                            
                  }              
