var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Bids = new Schema({
  BidderName: String,
  Technology:String,
  JobUrl: String,
  JobPortal: String,
  JobId: String,
  IsInvite:String,
  Status: String,
  BidType:String,
  date: Date,
  pagenum: Number,
}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

mongoose.model('Bids', Bids);