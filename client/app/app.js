/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";

angular.module("myApp", [
  'ngResource',
  'myApp.filters',
  'myApp.ProjectServices',
  'myApp.WorkroomServices',
  'myApp.BidServices',
  'myApp.LeadServices',
  'myApp.ClientServices',
  'myApp.UserServices',
  'myApp.DeveloperServices',
  'myApp.KeywordServices',
  'myApp.ProjectKeywordServices',
  'myApp.directives',
  'myApp.controllers'
]);


var app=angular.module("myApp.controllers", ['ngRoute', 'ngTagsInput','ngClipboard','ui.bootstrap']);
var ProjectService=angular.module("myApp.ProjectServices", []);
var WorkroomService=angular.module("myApp.WorkroomServices", []);
var BidsService=angular.module("myApp.BidServices", []);
var LeadService=angular.module("myApp.LeadServices", []);
var ClientService=angular.module("myApp.ClientServices", []);
var UserService=angular.module("myApp.UserServices", []);
var DeveloperService=angular.module("myApp.DeveloperServices", []);
var KeywordServices=angular.module("myApp.KeywordServices", []);
var ProjectKeywordServices=angular.module("myApp.ProjectKeywordServices", []);
  _.mixin(_.string.exports());