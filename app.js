var express = require('express'), mongoose = require('mongoose'), fs = require('fs'), http = require('http'),
	config = require('./config/config'), root = __dirname, app = express(), server = null;
var lessMiddleware = require('less-middleware');
var path=require('path');
// Configuration
require('./config/db')(config);

var modelsPath = __dirname + '/server/models';
fs.readdirSync(modelsPath).forEach(function (file) {
  if (file.indexOf('.js') >= 0) {
    require(modelsPath + '/' + file);
  }
});

   //session management=====================================
		app.use(express.bodyParser());                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
		app.use(express.cookieParser('shhhh, very secret'));  
		app.use(express.session());   
    
  //less
// app.use(express.static( '/client/assets'));
// app.use(lessMiddleware('/client/assets'))
app.use(lessMiddleware(path.join(__dirname, 'client')));
app.use(express.static(path.join(__dirname, 'client')));

// app.configure(function(){
//   //other configuration here...
//   app.use(lessMiddleware({
//     src      : __dirname + "/client",
//     compress : true
//   }));
//   app.use(express.static(__dirname + '/client'));
// });


require('./config/express')(app, config);
require('./config/routes')(app);

var server = http.createServer(app);
server.listen(config.port, config.host);
console.log('App started on port ' + config.port);
