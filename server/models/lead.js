var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Lead = new Schema({
  JobId : String,
  JobTitle: String,
  ApplicationUrl: String,
  MessageUrl: String,
  Interviews: Number,
  BidAmount:Number,
  LeadType: String,
  CurrentStatus: String,
  ClientName:String,
  date: Date,
  ModifiedDate:Date,
  pagenum:Number,
}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

mongoose.model('Lead', Lead);