 // Factory to hit All Users related RESTFUL API===========================*****//

UserService.factory("User",   function($resource) {  
    return $resource('/api/users', {
      }, {
        login: {
          method:"POST"
        },
        logout: {
        	method:"GET"
        }
    });
})


UserService.factory("UsersIsAuthorised",   function($resource) {  
    return $resource('/api/usersisauthorised', {
      }, {
        isAuthorised: {
           method:"GET"
        }
    });
})