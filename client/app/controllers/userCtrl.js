/*jslint node: true */
/*jslint nomen: true */
/*global angular, _ */
"use strict";



angular.module("myApp.controllers").controller("userCtrl", function($scope, $timeout,User,UsersIsAuthorised, $rootScope,$location) {

    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse:Login Method
   $("#cntntData").hide();

  $scope.gethomepage = function(User){          
       $("#loginpage").hide();
      var user = new UsersIsAuthorised();      
      user.$isAuthorised(function(response) {
      window.history.forward(1);
       if(response.status=='true')
       {            
       $('#menubar').attr('style','display:');
        $scope.menuActive();
        $location.path('/projectlist');        
       }
       else
       { 
        $("#loginpage").show();
         $('#menubar').attr('style','display:none');
         $('body').addClass('login_bg');
         $scope.menuActive();
          $location.path('/');
       }
      });
  }



   $scope.checkLogin = function( username,password) {
    if(username=='' || username=='undefined' || username==null)
    {
      alert($rootScope.login_user_msg);
    }
    else if(password=='' || password=='undefined' || password==null)
    {

      alert($rootScope.login_passwrd_msg);
    }
    else
    {
    var user = new User();
    user.UserName=username;
    user.Password=password;
          
          user.$login(function(response) {
            
            
               if(response.status=="true")
               { 
                  
                     $("#cntntData").show();
 
                  // Timer is used  to solve issue of Template Rendring
                  var waitToRenderHTML = $timeout(function myFunction() {
                       $scope.userName=response.users.UserName;
                        $('body').removeClass('login_bg');
                       $('#menubar').attr('style','display:');
                       $('#loginpage').hide();
  $location.replace();
                  $location.path('/projectlist');
                   },3000);


                
               }
               else
               {

               // $scope.newUser.UserName="";
                $scope.newUser.Password="";
                alert($rootScope.user_login_err);
               }
        
       });  
     }
  };


  

    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse:Logout Method

   $scope.logout = function( users) {
      var user = new User();
      
      user.$logout(function(response) {
       if(response.status=='true')
       {
           //$scope.loggedin=false;
          // $('#menubar').hide();
         $('body').addClass('login_bg');

          $('#menubar').attr('style','display:none'); 
           //    var backlen=history.length;
           // history.go(-backlen);
           $('#loginpage').show();
           $location.replace();
          $location.url('/',true);
       }
      });
   };


    // Created By: Parveen Yadav
    // Date: 10/07/2014
    // Puropse:Logout Method

   $scope.isauthorised = function( users) {
      var user = new UsersIsAuthorised();
      user.$isAuthorised(function(response) {
 window.history.forward(1);
       if(response.status=='true')
       {
         $('#menubar').attr('style','display:');
        $scope.menuActive();           
       }
       else
       {
   
         $('#menubar').attr('style','display:none');
         $('body').addClass('login_bg');
         $scope.menuActive();
          $location.path('/');
       }


      });
   };
   

    // Created By: Parveen Yadav
    // Date: 18/07/2014
    // Puropse:Make LHS menu Active

   $scope.menuActive = function() {
      
      jQuery('a').click(function (event) {
    var id = $(this).attr("id");
    if(id=='addProject')
    {
      $('#addProject').addClass('active');
      $('#leads').removeClass('active');
      $('#leadsToday').removeClass('active');
      $('#leadsPrevious').removeClass('active');
      $('#todayBids').removeClass('active');
      $('#search').removeClass('active');
      $('#previousBids').removeClass('active');
      

    }
    else if(id=='leads')
    {
       $('#leads').addClass('active');
      $('#addProject').removeClass('active');
      $('#leadsPrevious').removeClass('active');
      $('#leadsToday').removeClass('active');
      $('#bids').removeClass('active');
      $('#todayBids').removeClass('active');
      $('#search').removeClass('active');
      $('#previousBids').removeClass('active');
      

      
    }
    else if(id=='leadsToday')
    {
       $('#leadsToday').addClass('active');
      $('#addProject').removeClass('active');
      $('#leads').removeClass('active');
      $('#leadsPrevious').removeClass('active');
      $('#bids').removeClass('active');
      $('#todayBids').removeClass('active');
      $('#search').removeClass('active');
      $('#previousBids').removeClass('active');
       
      
    }
      else if(id=='leadsPrevious')
    {
       $('#leadsPrevious').addClass('active');
      $('#addProject').removeClass('active');
      $('#leads').removeClass('active');
      $('#leadsToday').removeClass('active');
      $('#bids').removeClass('active');
      $('#todayBids').removeClass('active');
      $('#search').removeClass('active');
      $('#previousBids').removeClass('active');
     

    }
       else if(id=='bids')
    {
      $('#bids').addClass('active');

      $('#leadsPrevious').removeClass('active');
      $('#addProject').removeClass('active');
      $('#leads').removeClass('active');
      $('#leadsToday').removeClass('active');
      $('#todayBids').removeClass('active');
      $('#search').removeClass('active');
      $('#previousBids').removeClass('active');
      
 
    }
     else if(id=='todayBids')
    {
      $('#todayBids').addClass('active');
      $('#bids').removeClass('active');
      $('#leadsPrevious').removeClass('active');
      $('#addProject').removeClass('active');
      $('#leads').removeClass('active');
      $('#leadsToday').removeClass('active');
      $('#search').removeClass('active');
      $('#previousBids').removeClass('active');


    }
       else if(id=='previousBids')
    {
      $('#previousBids').addClass('active');
      $('#todayBids').removeClass('active');
      $('#bids').removeClass('active');
      $('#leadsPrevious').removeClass('active');
      $('#addProject').removeClass('active');
      $('#leads').removeClass('active');
      $('#leadsToday').removeClass('active');
      $('#search').removeClass('active');

    }
    else if(id=='search')
    {
      $('#search').addClass('active');
      $('#previousBids').removeClass('active');
      $('#todayBids').removeClass('active');
      $('#bids').removeClass('active');
      $('#leadsPrevious').removeClass('active');
      $('#addProject').removeClass('active');
      $('#leads').removeClass('active');
      $('#leadsToday').removeClass('active');
    }
  else
  {}
 });
   };

});