var mongoose = require('mongoose');
var Schema = mongoose.Schema; 
var client_lead_join = new Schema({    
    value: {  

    	    JobId : String,
          JobTitle: String,
          ApplicationUrl: String,
          MessageUrl: String,
          Interviews: Number,
          BidAmount:Number,
          LeadType: String,
          CurrentStatus: String,
          ClientName: String,
          ClientEmail: String,
          ClientBudget: Number,
          ClientSkype: String,
          ClientHistory:String,
              
           }  
});  

module.exports = mongoose.model('client_lead_join',client_lead_join);  