var mongoose = require('mongoose'),Bids = mongoose.model('Bids');
var moment = require('moment');
var Lead = mongoose.model('Lead');
var client = mongoose.model('Client');
var ClientLead = mongoose.model('client_lead_join');
mongoose.set('debug', true);

//Created By: Sachin Sharma
//Dated: 28/7/2014
// Save bid data in mongoose database
exports.savebid = function(req, res) {
  //console.log("here 1--.",req.body);
  var document = new Bids(req.body);
  Bids.find({
    "JobId": req.body.JobId
  }, function(err, data) {
    //console.log("data-->",data,data.length);
    if (data.length > 0) {
      res.json({
        "message": "This Job already exists"
      })
    } else {
      document.save(function(err, bid) {
        if (err) {
          res.json({
            "status": "false",
            "error": err
          });
        } else {
          res.json({
            "status": "true",
            "bid": bid
          });
        }
      });
    }
  });
  // document.save(function(err, bid) {
  //    if (err) {
  //      res.json({"status":"false","error":err}); 
  //     }else{
  //      res.json({"status":"true","bid":bid});
  //     }
  // });
}; //*******saveBid function ends here*******//



//save client lead data to clientlead collection.
  exports.saveclientlead = function(req, res) {
  console.log("here 1--.",req.body);
  var document = new ClientLead(req.body); 
  document.save(function(err, clientlead) {
        if (err) {
          res.json({
            "status": "false",
            "error": err
          });
        } else {
          res.json({
            "status": "true",
            "clientlead": clientlead
          });
        }
      });
    };
 
//Created By: Sachin Sharma
//Dated: 28/7/2014
// Get all bid data from mongoose database
exports.getAllBids = function(req, res) {
  console.log("get all bids here: ", req.session, req.body);
  var username = req.session.username;
  var datalimit = 50;
  Bids.find({
    "BidType": "bid"
  }).count(function(err, count) {
    if ((err == null) && (count != null))
      var count = count;

    Bids.find({
      "BidType": "bid",
      "BidderName": username
    }).count(function(err, mybidscount) {
      if ((err == null) && (mybidscount != null))
        var mybidscount = mybidscount;

      if (typeof req.body != undefined || req.body != '') {
        if (req.body.pagenum) {
          var pagenum = req.body.pagenum;
          var skipcount = (pagenum * datalimit) - datalimit;
        } else {
          var pagenum = 0
          var skipcount = 0
        }
      } else {
        var skipcount = 0;
      }
      //console.log("count",count);
      //var count = count;
      // var mybidscount = mybidscount;
      // db.bids.find().limit(datalimit).skip(skipcount)
      Bids.find({
        "BidderName": username,
        "BidType": "bid"
      }).limit(datalimit).skip(skipcount).sort({
        date: -1
      }).exec(function(err, specificbids) {
        if ((err == null) && (specificbids != null))
          data = {
            "specificbids": specificbids
          }

        Bids.find({
          "BidType": "bid"
        }).limit(datalimit).skip(skipcount).sort({
          date: -1
        }).exec(function(err, allbids) {
          var json = {};
          if ((err == null) && (allbids != null)) {
            json = {
                "allbids": allbids
              }
              //console.log(json);
            res.json({
              "status": "true",
              "result": json,
              "uname": username,
              "count": count,
              "pagenum": pagenum,
              "specific": specificbids,
              "mybidscount": mybidscount
            });
          } else {
            res.send({
              "status": "false",
              "error": err
            });
          }
        });
      });
    });
  });
}






exports.searchbidbyjoburl = function(req, res) {
  //console.log("get all bids here: ", req.session, req.body);
  var username = req.session.username;
  var JobUrl=req.params.JobUrl.trim();
  console.log("HHHHhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh::",JobUrl);
  var datalimit = 50;
  Bids.find({
    "BidType": "bid",
   // "JobUrl": {'$regex' : '.*' + JobUrl + '.*'}
    "JobUrl": JobUrl
  }).count(function(err, count) {
    if ((err == null) && (count != null))
      var count = count;

    Bids.find({
      "BidType": "bid",
      "BidderName": username,
       "JobUrl": JobUrl
    }).count(function(err, mybidscount) {
      if ((err == null) && (mybidscount != null))
        var mybidscount = mybidscount;

      if (typeof req.body != undefined || req.body != '') {
        if (req.body.pagenum) {
          var pagenum = req.body.pagenum;
          var skipcount = (pagenum * datalimit) - datalimit;
        } else {
          var pagenum = 0
          var skipcount = 0
        }
      } else {
        var skipcount = 0;
      }
      //console.log("count",count);
      //var count = count;
      // var mybidscount = mybidscount;
      // db.bids.find().limit(datalimit).skip(skipcount)
     //db.bids.find({"JobUrl" : {$regex : ".*.*"}});
      Bids.find({
         "BidderName": username,
        "BidType": "bid",
         "JobUrl":JobUrl   
      }).limit(datalimit).skip(skipcount).sort({
        date: -1
      }).exec(function(err, specificbids) {
        if ((err == null) && (specificbids != null))
          data = {
            "specificbids": specificbids
          }

        Bids.find({

        "BidType": "bid",
           "JobUrl": JobUrl
        }).limit(datalimit).skip(skipcount).sort({
          date: -1
        }).exec(function(err, allbids) {
          var json = {};
          if ((err == null) && (allbids != null)) {
            json = {
                "allbids": allbids
              }
              //console.log(json);
            res.json({
              "status": "true",
              "result": json,
              "uname": username,
              "count": count,
              "pagenum": pagenum,
              "specific": specificbids,
              "mybidscount": mybidscount
            });
          } else {
            res.send({
              "status": "false",
              "error": err
            });
          }
        });
      });
    });
  });
}







//Created By: Sachin Sharma
//Dated: 29/7/2014
// Delete a  bid data from mongoose database
exports.deletebid = function(req, res) {
  Bids.remove({
    "JobId": req.params.JobId
  }, function(err, bid) {
    if (err) {
      throw new Error({
        "status": "false",
        "error": err
      });
    }
    res.send({
      "status": "true",
      "bids": bid
    });
  });
};

//Created By: Vimal Sharma
//Dated: 10/3/2015
// Get bid data by Jobid.
exports.duplicatebid = function(req, res) {

  console.log("get all bids here: ", req.session, req.body);
  Bids.find({
    "JobId": req.params.JobId
  }).count(function(err, count){
    if (err) {
      throw new Error({
        "status": "false",
        "error": err
      });
    }
    res.send({
      "status": "true",
      "counts": count
    });
  });
};

//Created By: Vimal Sharma
//Dated: 10/3/2015
// Get bid data by Jobid.
exports.bidbyjoburl = function(req, res) {
  console.log("get all bids here: ", req.session, req.body);
  Bids.find({
    "JobUrl": req.params.JobUrl
  }).exec(function(err, bid) {
          var json = {};
          if ((err == null) && (bid != null)) {
            json = {
                "bid": bid
              }
              //console.log(json);
            res.json({
              "status": "true",
              "result": json           
            });
          } else {
            res.send({
              "status": "false",
              "error": err
            });
          }
        });
};







//*******removebid function end here*******//

//Created By: Sachin Sharma
//Dated: 29/7/2014
// Change a bid status in bids collection
exports.changestatus = function(req, res) {
  console.log("inside change status", req.body);
  var status = req.body.Status;
  var stat = '';
  if (status == 'Applying') {
    stat = 'Applied';
  } else {
    stat = 'Applying';
  }

  Bids.update({
    "JobId": req.params.JobId
  }, {
    $set: {
      Status: stat
    }
  }, function(err, result) {
    if (err) {
      res.json({
        "status": "false",
        "error": err
      });
    } else {
      console.log("result--", result);
      res.json({
        "status": "true",
        "data": result
      });
    }
  });
}

//Created By: Sachin Sharma
//Dated: 30/7/2014
// Get today's bids from bids collection
exports.getTodaysBids = function(req, res) {
  console.log("her----->",req.body);
  var date = new moment();
  var newdate = new moment();
  var str = " 00:00:00";
  var tdate = date.format('YYYY-MM-DD') + str;
  var username = req.session.username;
  var newdate = new moment(tdate);
  Bids.find({
    "BidType": "bid",
    "date": {
      $gte: newdate.toDate()
    }
  }).sort({
    date: 1
  }).exec(function(err, bids) {
    var json = {};
    if ((err == null) && (bids != null)) {
      json = {
        "bids": bids
      }
      console.log(json);
      res.json({
        "status": "true",
        "result": json,
        "uname": username
      });
    } else {
      res.send({
        "status": "false",
        "error": err
      });
    }

  });
}

//Created By: Sachin Sharma
//Dated: 30/7/2014
// Get Previous Seven days  bids from bids collection
exports.getPrevSevenDaysBids = function(req, res) {
  console.log("here inside get previous", req.body);
  var date = new moment();
  var newdate = new moment();
  var str = " 00:00:00";
  var tdate = date.format('YYYY-MM-DD') + str;
  var username = req.session.username;
  var newdate = new moment(tdate);
  newdate.subtract('days', 7);
  Bids.find({
    "BidType": "bid",
    "date": {
      $gte: newdate.toDate()
    }
  }).sort({
    date: 1
  }).exec(function(err, bids) {
    var json = {};
    if ((err == null) && (bids != null)) {
      json = {
        "bids": bids
      }
      console.log(json);
      res.json({
        "status": "true",
        "result": json,
        "uname": username
      });
    } else {
      res.send({
        "status": "false",
        "error": err
      });
    }

  });
}

//Created By: Sachin Sharma
//Dated: 31/7/2014
// Change Bid type from bid to lead
exports.changeToLead = function(req, res) {
  var JobId = req.body.JobId;
  var type = "lead"
  Bids.update({
    "JobId": req.params.JobId
  }, {
    $set: {
      BidType: type
    }
  }, function(err, result) {
    if (err) {
      res.json({
        "status": "false",
        "error": err
      });
    } else {
      res.json({
        "status": "true",
        "data": result
      });
      ClientLeadJoin();
    }
  });
}

function ClientLeadJoin() {
  var o = {};
  o.map = function() {
    if (this.JobId != null) {
      print(this.JobId);
      var output = {
        _id: this._id,
        ApplicationUrl: this.ApplicationUrl,
        JobTitle: this.JobTitle,
        JobId: this.JobId,
        MessageUrl: this.MessageUrl,
        Interviews: this.Interviews,
        CurrentStatus: this.CurrentStatus,
        LeadType: this.LeadType,
        BidAmount: this.BidAmount,
        ClientName: db.clients.findOne({
          "JobId": this.JobId
        }).ClientName,
        ClientEmail: db.clients.findOne({
          "JobId": this.JobId
        }).ClientEmail,
        ClientBudget: db.clients.findOne({
          "JobId": this.JobId
        }).ClientBudget,
        ClientSkype: db.clients.findOne({
          "JobId": this.JobId
        }).ClientSkype,
        ClientHistory: db.clients.findOne({
          "JobId": this.JobId
        }).ClientHistory
      }
      emit(this._id, output);
    }
  }
  o.reduce = function(key, values) {
    var outs = {
      _id: null,
      ApplicationUrl: null,
      JobTitle: null,
      MessageUrl: null,
      Interviews: null,
      CurrentStatus: null,
      LeadType: null,
      BidAmount: null,
      ClientName: null
    }
    values.forEach(function(v) {
      if (outs._id != null) {
        if (outs.ApplicationUrl == null) {
          outs.ApplicationUrl = v.ApplicationUrl
        }
        if (outs.JobTitle == null) {
          outs.JobTitle = v.JobTitle
        }
        if (outs.MessageUrl == null) {
          outs.MessageUrl = v.MessageUrl
        }
        if (outs.Interviews == null) {
          outs.Interviews = v.Interviews
        }
        if (outs.CurrentStatus == null) {
          outs.CurrentStatus = v.CurrentStatus
        }
        if (outs.LeadType == null) {
          outs.LeadType = v.LeadType
        }
        if (outs.BidAmount == null) {
          outs.BidAmount = v.BidAmount
        }
        if (outs.ClientName == null) {
          outs.ClientName = v.ClientName
        }
      }
    });
    return outs;
  }
  o.out = {
    replace: 'client_lead_joins'
  }
  o.verbose = true;
  Lead.mapReduce(o, function(err, results) {
    console.log(err)
  })
}