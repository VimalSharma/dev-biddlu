var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Workroom = new Schema({
  url: String,
  date: String,
  bidType: String
}, {
  versionKey: false,
  
  toJSON: {
    virtuals: true,
    transform: function(doc, ret, options) {
      ret.id = ret._id.toHexString();
      delete ret._id;
    }
  },
  
  toObject: {
    virtuals: true
  }
});

mongoose.model('Workroom', Workroom);
