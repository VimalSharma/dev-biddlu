// TO Define HTML Page Routing========================================================
angular.module("myApp.controllers").config(['$routeProvider', function($routeProvider) {

  $routeProvider.when('/', {
    templateUrl: 'views/user/login.html', 
    controller: 'userCtrl'
  }).when('/addproject',{
    templateUrl:'views/project/AddProject.html',
    controller:'projectCtrl'
  }).when('/projectlist',{
     templateUrl:'views/project/projectlist.html',
    controller:'projectCtrl'
  }).when('/logout',{
    templateUrl:'views/user/login.html',
    controller:'userCtrl' 
  }).when('/home',{
    templateUrl:'views/home/search.html',
    controller:'userCtrl' 
  }).when('/workroom',{
    templateUrl:'views/workroom/workroom.html',
    controller:'workroomCtrl' 
  }).when('/editproject',{
    templateUrl:'views/project/editproject.html',
    controller:'projectCtrl'
  }).when('/recentbids',{
    templateUrl:'views/workroom/bid.html',
    controller:'bidCtrl'
  }).when('/recentleads',{
    templateUrl:'views/workroom/lead.html',
    controller:'leadCtrl'
  });

  $routeProvider.otherwise({redirectTo: '/'});

}]);