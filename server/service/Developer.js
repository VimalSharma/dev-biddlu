var mongoose = require('mongoose'), Model = mongoose.model('Developer');
mongoose.set('debug', true);


			//TO search Keywords
			exports.developerautocomplete = function(req, res) {
			  Model.find({text: new RegExp(req.query.key, 'i')}, function(err, the_keywords){
			    if(the_keywords.length > 0){
			    	res.send(the_keywords);
			    }else{
			      	res.send({"status":"false","error":err}); 
			    }
			  });
			}//************


	//Get  Developer  list by their developer Id
       exports.getDeveloperBydevid = function(req, res) {
        Model.findById(req.params.id, function(err, developers) {
      
          if((err==null ) && (developers!=null)){
              var json={};
              json={"DeveloperList":developers}
              console.log(JSON.stringify(developers));
               res.send({"status":"true"},json); 
          }
          else
          {
           res.send({"status":"false","error":err}); 
          }
        });
      };//*******//