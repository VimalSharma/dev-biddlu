//Add this to have access to a global variable
app.run(function ($rootScope) {
    
    //create By: Parveen Yadav
    //Date : 10/07/2014
    //purpose: validation message goes here
    $rootScope.user_login_err = 'Authentication failed, please re-check your User Name and Password';
    
    $rootScope.Project_Add_Msg = 'Project Added Sucessfully';
    $rootScope.Project_Deletion_Msg = 'Project Deleted Sucessfully';
    $rootScope.Project_Updation_Msg = 'Project Updated Sucessfully';
    $rootScope.Project_Search_Err = 'No project found with this Keyword name. ';
    
    //create By: Sachin Sharma
    //Date : 29/07/2014
    //purpose: validation message goes here
    $rootScope.form_Empty_Message='Please Fill Empty fields';
    $rootScope.required_Fields_Msg='Please Fill all required fields.';
    $rootScope.invalid_Odesk_Url_Message='Please Enter a valid odesk or elance Job Url'
    $rootScope.invalid_Url_Message='Please enter a valid Url in Application URL . '
    $rootScope.invalid_Alphabetic_Message='Please enter a valid alphabetic name in client name'
    $rootScope.invalid_Number_Message="Please enter a valid number in numeric fields"
    $rootScope.invalid_Email_Message="Please enter a valid email"

    //create By: Sachin Sharma
    //Date : 05/08/2014
    //purpose: Bids Success message goes here
    $rootScope.Bid_Add_Msg='Bid added Succesfully';
    $rootScope.Job_Exist_Msg='This Job already Exists';
    $rootScope.Bid_Deletion_Msg='Bid Deleted Successfully';
    $rootScope.Bid_StatusChange_Msg='Bid Status Changed Succesfully';
    $rootScope.Bid_ToLead_Msg='Bid Converted to Lead Succesfully';

    //create By: Sachin Sharma
    //Date : 05/08/2014
    //purpose: Lead Success message goes here
    $rootScope.Lead_Deletion_Msg='Lead Deleted Successfully';
    $rootScope.Lead_Edit_Msg='Lead Edited Successfully';
     
    //create By: Parveen Yadav
    //Date : 18/08/2014
    //purpose: Login Message
    $rootScope.login_user_msg='Please Fill User Name';
    $rootScope.login_passwrd_msg='Please Fill Password';

    //create By: Vimal Sharma
    //Date : 21/08/2014
    //purpose: Search Message
    $rootScope.search_msg='Please Enter Keyword';

});
